# Maintenance

## Creating a Release
Creating a release is a little bit involved. The following checklist explains
the process that should be followed for a smooth release.

1. **Check the documentation** Read through the documentation in
docs/usermanual. Make sure that everything is up to date and accurate; all
major features added in the release are documented thoroughly; no screenshots
contain UI elements that have since changed (even if they aren't the focus of
the screenshot).
2. **Update dependencies** In the flatpak manifest, make sure the runtime is
on its latest stable version. Make sure everything still works, then commit the
new manifest.
3. **Write a changelog** Read through the commit history since the last
release and summarize it in a changelog. See Changelog Formatting below. Do
**not** commit the changelog yet!
4. **Create the release commit** In this commit, update the NEWS file with the
changelog entry created earlier. Also, update the version number in
meson.build. Don't do anything else in this commit. Tag the commit with the
version number. The commit message must be of the form `RELEASE: <version>`
with no further description.
5. **Push the commit** Push the commit to GitLab! CI will take care of
(most of) the rest: publishing the new downloads, updating the user manual, etc.
6. **Update Flathub** Go to the flathub-floodlight repository and copy the new
flatpak manifest there. Change the tag/commit at the bottom to match the commit
created above. Submit the changes to flathub.
7. **Publish the changelog on the blog** Feel free to elaborate more than in
the NEWS file. This is a blog post, after all. Explain the hard work that went
into the release, the story behind a particular feature, etc.
8. **Get the word out** Announce the release on social media. Link to the blog
post. Make sure people who use Floodlight know about it, and use it as an
opportunity to spread the word about the project.

### Changelog Formatting
The NEWS file is intended to be machine readable. Make sure to copy the format
of previous entries. When in doubt, consult src/photon/updates.vala, as this is
the code responsible for parsing the file.

```
<version>
=========

Released: <date, YYYY-MM-DD>

New Features:
- <markdown list>

Changes:
- <markdown list>
```
