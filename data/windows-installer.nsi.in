# From https://nsis.sourceforge.io/A_simple_installer_with_start_menu_shortcut_and_uninstaller

!define VERSION @VERSION@
!define HELPURL "http://floodlight.gitlab.io/presenter/usermanual"
!define UPDATEURL "http://floodlight.gitlab.io"
!define ABOUTURL "http://floodlight.gitlab.io"

RequestExecutionLevel admin

InstallDir "$PROGRAMFILES\Floodlight\Presenter"

LicenseData "COPYING"
# This will be in the installer/uninstaller's title bar
Name "Floodlight Presenter"
Icon "presenter.ico"
outFile "InstallFloodlight.exe"

!include LogicLib.nsh

# Just three pages - license agreement, install location, and installation
page license
page directory
Page instfiles

!macro VerifyUserIsAdmin
UserInfo::GetAccountType
pop $0
${If} $0 != "admin" ;Require admin rights on NT4+
        messageBox mb_iconstop "Administrator rights required!"
        setErrorLevel 740 ;ERROR_ELEVATION_REQUIRED
        quit
${EndIf}
!macroend

function .onInit
	setShellVarContext all
	!insertmacro VerifyUserIsAdmin
functionEnd

section "install"
	# Files for the install directory - to build the installer, these should be in the same directory as the install script (this file)
	setOutPath $INSTDIR
	# Files added here should be removed by the uninstaller (see section "uninstall")
	file /r "bin"
	file /r "resources"
	file "presenter.ico"
	file "COPYING"
	# Add any other files for the install directory (license files, app data, etc) here

	# Uninstaller - See function un.onInit and section "uninstall" for configuration
	writeUninstaller "$INSTDIR\UninstallFloodlight.exe"

	# Start Menu
	createShortCut "$SMPROGRAMS\Floodlight.lnk" "$INSTDIR\bin\floodlight-presenter.exe" "" "$INSTDIR\presenter.ico"

	# Registry information for add/remove programs
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\FloodlightPresenter" "DisplayName" "Floodlight Presenter"
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\FloodlightPresenter" "UninstallString" "$\"$INSTDIR\UninstallFloodlight.exe$\""
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\FloodlightPresenter" "QuietUninstallString" "$\"$INSTDIR\UninstallFloodlight.exe$\" /S"
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\FloodlightPresenter" "InstallLocation" "$\"$INSTDIR$\""
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\FloodlightPresenter" "DisplayIcon" "$\"$INSTDIR\presenter.ico$\""
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\FloodlightPresenter" "HelpLink" "${HELPURL}"
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\FloodlightPresenter" "URLUpdateInfo" "${UPDATEURL}"
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\FloodlightPresenter" "URLInfoAbout" "${ABOUTURL}"
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\FloodlightPresenter" "DisplayVersion" "${VERSION}"
	# There is no option for modifying or repairing the install
	WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\FloodlightPresenter" "NoModify" 1
	WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\FloodlightPresenter" "NoRepair" 1
sectionEnd

# Uninstaller

function un.onInit
	SetShellVarContext all

	#Verify the uninstaller - last chance to back out
	MessageBox MB_OKCANCEL "Permanantly remove Floodlight Presenter?" IDOK next
		Abort
	next:
	!insertmacro VerifyUserIsAdmin
functionEnd

section "uninstall"

	# Remove Start Menu launcher
	delete "$SMPROGRAMS\Floodlight.lnk"

	# Remove files
	RMDir /r "$INSTDIR\bin"
	RMDir /r "$INSTDIR\resources"
	delete "$INSTDIR\presenter.ico"
	delete "$INSTDIR\COPYING"

	# Always delete uninstaller as the last action
	delete "$INSTDIR\UninstallFloodlight.exe"

	# Try to remove the install directory - this will only happen if it is empty
	RMDir "$INSTDIR"

	# Remove uninstaller information from the registry
	DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\FloodlightPresenter"
sectionEnd
