int floodlight_self_test(string[] args) {
    Test.init(ref args, "isolate_dirs");

    add_photon_tests();

    add_cellar_tests();
    add_cellar_lyrics_import_tests();

    return Test.run();
}

// Utility functions

/**
 * Gets an arbitrary item from a collection.
 */
T get_item_in_collection<T>(Gee.Collection<T> s) {
    foreach (T item in s) return item;
    assert_not_reached();
}
