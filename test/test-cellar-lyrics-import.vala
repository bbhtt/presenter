const string FREEFORM = """
Amazing Grace

[Chorus 1]
Amazing grace! (how sweet the sound)
That sav'd a wretch like me!
I once was lost, but now am found,
Was blind, but now I see.

Verse
'Twas grace that taught my heart to fear,
And grace my fears reliev'd;
How precious did that grace appear
The hour I first believ'd!

Chorus 1

Thro' many dangers, toils, and snares,
I have already come;
'Tis grace hath brought me safe thus far,
And grace will lead me home.
'Tis grace hath brought me safe thus far,
And grace will lead me home.

[Chorus 1]


The Lord has promis'd good to me,
His word my hope secures;
He will my shield and portion be
As long as life endures.

(VERSE 3)
Yes, when this flesh and heart shall fail,
And mortal life shall cease;
I shall possess, within the veil,
A life of joy and peace.

Yes, when this flesh and heart shall fail,
And mortal life shall cease;
I shall possess, within the veil,
A life of joy and peace.

The earth shall soon dissolve like snow,
The sun forbear to shine;
But God, who call'd me here below,
Will be forever mine.

The earth shall soon dissolve like snow,
The sun forbear to shine;
But God, who call'd me here below,
Will be forever mine.

Tag:
You are forever mine




""";

void add_cellar_lyrics_import_tests() {
    Test.add_func("/cellar/lyrics-import/freeform", () => {
        var resource = new Cellar.Resource.from_scratch(Cellar.ResourceType.SONG, "Song from Clipboard", "");
        var slideshow = Cellar.ImportUtils.parse_lyrics(FREEFORM, resource);

        assert_true(resource.name == "Amazing Grace");

        var arrangement = slideshow.arrangement;
        var slides = arrangement.get_slides();
        var contents = arrangement.get_contents();

        assert_true(contents.size == 11);
        assert_true(slides.size == 12);

        assert_true(contents[0].group.name == "Chorus 1");
        assert_true(contents[0].group.get_slides()[0].get_field("content") == """Amazing grace! (how sweet the sound)
That sav'd a wretch like me!
I once was lost, but now am found,
Was blind, but now I see.""");

        assert_true(contents[1].group.name == "Verse");
        assert_true(contents[1].group.get_slides()[0].get_field("content") == """'Twas grace that taught my heart to fear,
And grace my fears reliev'd;
How precious did that grace appear
The hour I first believ'd!""");

        assert_true(contents[2].group == contents[0].group);

        assert_true(contents[3].group.name == "Group 1");
        assert_true(contents[3].group.get_slides().size == 2);
        assert_true(contents[3].group.get_slides()[0].get_field("content") == """Thro' many dangers, toils, and snares,
I have already come;
'Tis grace hath brought me safe thus far,""");
        assert_true(contents[3].group.get_slides()[1].get_field("content") == """And grace will lead me home.
'Tis grace hath brought me safe thus far,
And grace will lead me home.""");

        assert_true(contents[4].group == contents[0].group);

        assert_true(contents[5].group.name == "Group 2");
        assert_true(contents[5].group.get_slides()[0].get_field("content") == """The Lord has promis'd good to me,
His word my hope secures;
He will my shield and portion be
As long as life endures.""");

        assert_true(contents[6].group.name == "VERSE 3");
        assert_true(contents[6].group.get_slides()[0].get_field("content") == """Yes, when this flesh and heart shall fail,
And mortal life shall cease;
I shall possess, within the veil,
A life of joy and peace.""");

        assert_true(contents[7].group == contents[6].group);

        assert_true(contents[8].group.name == "Chorus 2");
        assert_true(contents[8].group.get_slides()[0].get_field("content") == """The earth shall soon dissolve like snow,
The sun forbear to shine;
But God, who call'd me here below,
Will be forever mine.""");

        assert_true(contents[9].group == contents[8].group);

        assert_true(contents[10].group.name == "Tag");
        assert_true(contents[10].group.get_slides()[0].get_field("content") == """You are forever mine""");
    });
}
