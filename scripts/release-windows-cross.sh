#!/bin/bash

# Sets up Meson to create a Windows installer for Floodlight, cross-compiled on
# Linux. The build directory will be _release_build.

# Typical usage:
#     ./scripts/release-windows.sh
#     ninja -C _release_build install
#     _release/Floodlight/InstallFloodlight.exe

mkdir -p _release/Floodlight
meson _release_build \
  --prefix $PWD/_release/Floodlight \
  --cross-file src/build/cross-mingw64.txt \
  --bindir bin \
  --libdir . \
  --datadir resources \
  -Dpackaged=true
