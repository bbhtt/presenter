#!/bin/bash

# Sets up Meson to create a Windows installer for Floodlight. The build
# directory will be _release_build.

# Typical usage:
#     ./scripts/release-windows.sh
#     ninja -C _release_build install
#     open _release/Floodlight.app

mkdir -p _release/Floodlight
meson _release_build \
  --prefix $PWD/_release/Floodlight \
  --bindir bin \
  --libdir . \
  --datadir resources \
  -Dpackaged=true
