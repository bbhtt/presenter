## Running the Release Scripts

To create a release, run the right `release-*.sh` script for your platform. It
will build a release and place it in the `_release` directory.
