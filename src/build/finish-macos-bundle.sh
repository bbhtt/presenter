#!/bin/bash

# Set up bundle structure
bundle=$MESON_INSTALL_PREFIX
executable=$bundle/Contents/MacOS/floodlight-presenter
resources=$bundle/Contents/Resources
libraries=$bundle/Contents/Libraries
# Make sure directories exist
mkdir -p $resources
mkdir -p $libraries


# Functions to copy necessary libraries and modify their paths
copied=""
copy_and_fix () {
  name=$(basename $1)
  dest=$libraries/$name

  # do not copy a library twice
  if echo "$copied" | grep -q "$name" ; then return; fi
  copied="$copied $name"

  cp $1 $dest
  chmod u+w $dest
  install_name_tool -change "$1" "$name" "$dest"
  install_name_tool -id "@rpath/$name" "$dest"
  fix "$dest"
}

fix () {
  deps="`otool -L "$1" | grep '\(local\|opt\)' | awk '{ print $1 }'`"
  for dep in $deps; do
    install_name_tool -change "$dep" "@rpath/`basename $dep`" "$1"
    copy_and_fix "$dep"
  done
}


# Copy all necessary libraries into the bundle
install_name_tool -add_rpath "@executable_path/../Libraries" $executable
fix $executable

# Copy GStreamer plugins from several different packages
gst_plugins=/usr/local/lib/gstreamer-1.0
for file in $gst_plugins/libgst*.{dylib.so} ; do
  copy_and_fix $file
done

# copy this gstreamer utility
gst_plugin_scanner=$(pkg-config --variable=prefix gstreamer-1.0)/libexec/gstreamer-1.0/gst-plugin-scanner
copy_and_fix $gst_plugin_scanner
install_name_tool -add_rpath "@executable_path" $libraries/gst-plugin-scanner


# Some prefixes we need to copy stuff from
gtk_prefix=$(pkg-config --variable=prefix gtk+-3.0)
gtk_immodules=$gtk_prefix/lib/gtk-3.0/$(pkg-config --variable=gtk_binary_version gtk+-3.0)/immodules
gdk_pixbuf_prefix=$(pkg-config --variable=prefix gdk-pixbuf-2.0)
gdk_loaders=$gdk_pixbuf_prefix/lib/gdk-pixbuf-2.0/$(pkg-config --variable=gdk_pixbuf_binary_version gdk-pixbuf-2.0)/loaders

# Copy gdk-pixbuf loaders
for file in $gdk_loaders/libpixbufloader-*.{dylib,so}; do
  copy_and_fix $file
done
# generate and fix gdk-loaders.cache
gdk-pixbuf-query-loaders \
  | sed "s|$gdk_loaders|@rpath|g" > $resources/gdk-loaders.cache

# Copy gtk immodules
for file in $gtk_immodules/im-*.{dylib,so}; do
  copy_and_fix $file
done
# generate and fix immodules.cache
gtk-query-immodules-3.0 \
  | sed "s|$gtk_immodules|@rpath|g" \
  | sed "s|$gtk_prefix|@executable_path/../Resources|g" \
  > $resources/immodules.cache
# copy locale data. for now, only copy english data, because that's the only
# language Floodlight itself supports
mkdir -p $resources/share/locale
cp -R $gtk_prefix/share/locale/en* $resources/share/locale

# Copy icons
# TODO: Copy only the files we need, instead of copying and then deleting
icon_dir=/usr/local/share/icons/Adwaita
mkdir -p $resources/share/icons
cp -R -L $icon_dir $resources/share/icons/Adwaita
# delete non-symbolic icons, which we don't use
find $resources/share/icons/Adwaita/* -type f ! -name "*symbolic.*" -a -name "*.png" -delete
# delete cursors directory, it's huge and doesn't seem to be important
rm -r $resources/share/icons/Adwaita/cursors
# For some reason, copying the hicolor theme's index.theme file fixes a couple
# missing icons, even though hicolor doesn't actually contain any icons
mkdir -p $resources/share/icons/hicolor
cp $icon_dir/../hicolor/index.theme $resources/share/icons/hicolor/index.theme

# GLib schemas
mkdir -p $resources/share/glib-2.0/schemas
glib-compile-schemas --targetdir=$resources/share/glib-2.0/schemas $gtk_prefix/share/glib-2.0/schemas

# Create ZIP file
cd $bundle/..
bundle_name=$(basename $bundle)
zip -r -q --symlinks $bundle_name.zip $bundle_name
cd -
