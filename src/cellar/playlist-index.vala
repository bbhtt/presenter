using Gee;

namespace Cellar {
    public class PlaylistIndex : Object {
        private static PlaylistIndex instance;
        public static PlaylistIndex get_instance() {
            if (instance == null) instance = new PlaylistIndex();
            return instance;
        }


        /**
         * Emitted when a new playlist is created.
         */
        public signal void playlist_added(PlaylistEntry playlist);

        /**
         * Emitted when the basic metadata (name, access time, folder, etc) of
         * a playlist is changed.
         */
        public signal void playlist_changed(PlaylistEntry playlist);

        /**
         * Emitted when a playlist is deleted.
         */
        public signal void playlist_removed(PlaylistEntry playlist);

        /**
         * Emitted when a folder is created.
         */
        public signal void folder_added(string uuid, string name);

        /**
         * Emitted when a folder is renamed.
         */
        public signal void folder_renamed(string uuid, string new_name);

        /**
         * Emitted when a folder is removed.
         */
        public signal void folder_removed(string uuid);


        private PlaylistEntry _open_playlist;
        /**
         * The entry for the playlist that is currently open.
         */
        public PlaylistEntry open_playlist {
            get {
                return this._open_playlist;
            }
            set {
                if (value == null) warning("Cannot set open-playlist to null!");
                else this._open_playlist = value;
                this._open_playlist.update_access_time();
            }
        }


        /**
         * A map of all playlist entries by their IDs.
         */
        private HashMap<string, PlaylistEntry> playlists;

        /**
         * A map of playlist folder names by their IDs.
         */
        private HashMap<string, string> folders;

        /**
         * The index file
         */
        private File file;

        /**
         * The directory where playlist files are stored
         */
        private File dir;

        private Json.Object json;


        private PlaylistIndex() {
            this.folders = new HashMap<string, string>();
            this.playlists = new HashMap<string, PlaylistEntry>();

            var config = Photon.Config.get_instance();
            this.file = File.new_for_path(config.datadir)
                        .get_child("playlists")
                        .get_child("index.json");
            this.dir = File.new_for_path(config.datadir)
                       .get_child("playlists");

            try {
                var parser = new Json.Parser();
                parser.load_from_file(this.file.get_path());
                this.json = parser.get_root().get_object();
            } catch (Error e) {
                debug("No playlist index found, starting anew");
                try {
                    var parser = new Json.Parser();

                    File defaults = File.new_for_uri("resource:///io/gitlab/floodlight/Presenter/defaults/playlist-index.json");
                    parser.load_from_stream(defaults.read());
                    this.json = parser.get_root().get_object();

                    this.save();

                    // copy the default playlist
                    File playlist = File.new_for_uri("resource:///io/gitlab/floodlight/Presenter/defaults/playlist.json");

                    try {
                        this.dir.make_directory_with_parents();
                    } catch(Error e) {
                        // already exists, not sure why, but ignore
                    }

                    File playlist_dest = this.dir.get_child(
                        Photon.DEFAULT_PLAYLIST + ".json"
                    );
                    playlist.copy(playlist_dest, FileCopyFlags.NONE);
                } catch (Error e2) {
                    critical("ERROR CREATING INITIAL PLAYLIST INDEX: %s", e2.message);
                }
            }

            this.json.get_object_member("playlists").foreach_member(
                (obj, key, val) => {
                    var entry = new PlaylistEntry(key, val.get_object());
                    this.add_internal(entry);
                }
            );

            this.json.get_object_member("folders").foreach_member(
                (obj, key, val) => {
                    this.folders[key] = val.get_object().get_string_member("name");
                }
            );

            this.open_playlist = this.get_playlist_entry(
                this.json.get_string_member("open-playlist")
            );
            this.notify["open-playlist"].connect((spec) => {
                this.json.set_string_member("open-playlist",
                                            this.open_playlist.uuid);
                this.save();
            });
        }


        /**
         * Gets a list of all #PlaylistEntry objects in the index.
         */
        public Gee.Collection<PlaylistEntry> get_playlists() {
            return this.playlists.values;
        }

        /**
         * Gets a #PlaylistEntry by UUID.
         */
        public PlaylistEntry? get_playlist_entry(string uuid) {
            return this.playlists.get(uuid);
        }

        /**
         * Adds a playlist to the index.
         *
         * To create a playlist, use #PlaylistEntry.from_scratch().
         */
        public void add_playlist(PlaylistEntry entry) {
            this.playlists[entry.uuid] = entry;
            this.json.get_object_member("playlists")
                     .set_object_member(entry.uuid, entry.json);
            this.add_internal(entry);

            this.playlist_added(entry);
            this.save();
        }

        /**
         * Gets a map of folder UUIDs to names.
         *
         * The returned HashMap is read-only.
         */
        public Map<string, string> get_folders() {
            return this.folders.read_only_view;
        }

        /**
         * Returns whether the folder name is valid.
         *
         * Folder names are just user-facing strings, not IDs, so technically
         * they can be anything. The rules only exist to avoid confusion.
         *
         * A folder name must not consist solely of whitespace, and it must not
         * match (case insensitively) the name of an existing folder.
         */
        public bool is_valid_folder_name(string name) {
            string canon = name.strip().casefold();
            if (canon == "") return false;
            foreach (string other in this.folders.values) {
                if (canon == other.strip().casefold()) return false;
            }
            return true;
        }

        /**
         * Adds a folder to the index with the given name.
         */
        public void add_folder(string name) {
            if (!this.is_valid_folder_name(name)) {
                warning("Folder name `%s` is not valid! It should have been checked before now.", name);
                return;
            }

            string uuid = Uuid.string_random();
            this.folders[uuid] = name;

            var folder = new Json.Object();
            folder.set_string_member("name", name);
            this.json.get_object_member("folders").set_object_member(uuid, folder);

            this.folder_added(uuid, name);
            this.save();
        }

        /**
         * Removes the folder with the given UUID.
         *
         * Playlists in this folder will be moved to the trash.
         */
        public void remove_folder(string uuid) {
            if (!this.folders.unset(uuid)) return;

            foreach (var entry in this.playlists.values) {
                if (entry.folder == uuid) {
                    // set folder to null so if the playlist is restored, it
                    // isn't placed in the now-deleted folder
                    entry.folder = null;
                    entry.trash = true;
                }
            }

            this.json.get_object_member("folders").remove_member(uuid);
            this.folder_removed(uuid);
            this.save();
        }

        public void rename_folder(string uuid, string new_name) {
            if (!this.is_valid_folder_name(new_name)) return;

            this.folders[uuid] = new_name;

            Json.Object folders = this.json.get_object_member("folders");
            if (folders.has_member(uuid)) {
                folders.get_object_member(uuid).set_string_member("name", new_name);
            } else {
                warning("Cannot rename folder %s which does not exist!", uuid);
            }

            this.folder_renamed(uuid, new_name);
            this.save();
        }

        /**
         * Deletes a playlist.
         *
         * The currently open playlist will not be deleted.
         */
        public void delete_playlist(PlaylistEntry entry) {
            if (entry == this.open_playlist) return;

            this.remove_internal(entry);
            this.json.get_object_member("playlists").remove_member(entry.uuid);
            this.playlist_removed(entry);

            this.save();
        }

        /**
         * Deletes all playlists marked as trash (except the open playlist).
         */
        public void empty_trash() {
            foreach (PlaylistEntry entry in this.playlists.values.to_array()) {
                if (entry.trash) this.delete_playlist(entry);
            }
        }


        /**
         * Adds the playlist entry to the in-memory map and connects to
         * all the right signals.
         */
        private void add_internal(PlaylistEntry entry) {
            entry.file = this.dir.get_child(entry.uuid + ".json");
            this.playlists[entry.uuid] = entry;
            entry.changed.connect(this.on_entry_changed);
        }

        /**
         * The opposite of add_internal()
         */
        private void remove_internal(PlaylistEntry entry) {
            this.playlists.unset(entry.uuid);
            entry.changed.disconnect(this.on_entry_changed);
        }

        private void on_entry_changed(PlaylistEntry entry) {
            this.playlist_changed(entry);
            this.save();
        }

        /**
         * Saves the index to disk.
         */
        private void save() {
            Photon.save_json(this.file, this.json);
        }
    }


    /**
     * Represents an entry in the playlist index.
     *
     * This does not contain the entire playlist, just the metadata stored in
     * the index. Use get_playlist() to obtain the full #Playlist object.
     */
    public class PlaylistEntry : Object {
        /**
         * Emitted when something about the playlist entry changes.
         */
        public signal void changed();


        /**
         * The playlist's UUID
         */
        public string uuid { get; private set; }

        private string _name;
        /**
         * The name of the playlist
         */
        public string name {
            get {
                return _name;
            }
            set {
                if (is_valid_playlist_name(value)) _name = value;
            }
        }

        /**
         * The UUID of the folder this playlist is in, or null if it is not in
         * a folder
         */
        public string? folder { get; set; }

        /**
         * Whether the playlist is in the trash.
         */
        public bool trash { get; set; default=false; }

        /**
         * The last time the playlist was opened, in UTC seconds.
         *
         * Cannot be set directly; use update_access_time().
         */
        public int64 last_access { get; private set; }


        /**
         * The file containing the full playlist data.
         */
        internal File file;

        internal Json.Object json;


        /**
         * Whether a playlist name is valid.
         */
        public static bool is_valid_playlist_name(string name) {
            return name.strip() != "";
        }


        internal PlaylistEntry(string uuid, Json.Object json) {
            this.uuid = uuid;
            this.json = json;

            this.name = json.get_string_member("name");
            this.last_access = json.get_int_member("last-access");
            if (json.has_member("folder")) this.folder = json.get_string_member("folder");
            if (json.has_member("trash")) this.trash = json.get_boolean_member("trash");

            this.notify["name"].connect((spec) => this.changed());
            this.notify["folder"].connect((spec) => this.changed());
            this.notify["trash"].connect((spec) => this.changed());
            this.changed.connect(() => {
                this.json.set_string_member("name", this.name);
                this.json.set_string_member("folder", this.folder);
                this.json.set_boolean_member("trash", this.trash);
                this.json.set_int_member("last-access", get_real_time() / 1000000);
            });
        }

        public PlaylistEntry.from_scratch(string name) {
            var json = new Json.Object();
            json.set_string_member("name", name);
            json.set_int_member("last-access", get_real_time() / 1000000);

            this(Uuid.string_random(), json);
        }


        /**
         * Loads the full #Playlist object for this #PlaylistEntry.
         *
         * No check is made that two instances of the Playlist are alive at
         * once; two calls to get_playlist() will return two different objects,
         * which may interfere with each other if they are edited. The design
         * of the application should prevent this from happening, though.
         */
        public Playlist get_playlist() {
            return new Playlist(this);
        }

        /**
         * Sets the last access time to the current time.
         */
        public void update_access_time() {
            this.changed();
        }
    }
}
