namespace Cellar {
    public static File library_files_dir;

    internal static bool initted = false;
    public void init() {
        if (initted) return;

        ResourceType.init();

        var config = Photon.Config.get_instance();
        library_files_dir = File.new_for_path(config.datadir)
                            .get_child("library")
                            .get_child("files");

        initted = true;
    }


    /**
     * Gets the filename for a resource, relative to the given prefix
     * directory (probably presenter-data/library/files).
     */
    internal File get_resource_file(string uuid) {
        return library_files_dir.get_child(uuid.substring(0, 2))
                                .get_child(uuid.substring(2));
    }

    /**
     * Gets the filename for a resource
     */
    internal File get_thumbnail_file(string uuid) {
        return library_files_dir.get_child(uuid.substring(0, 2))
                                .get_child("thumbnail-" + uuid.substring(2));
    }
}
