namespace Cellar {
    public class Slide : Object {
        public enum ContinueMode {
            /**
             * Do not continue automatically.
             */
            MANUAL,

            /**
             * Continue when all videos are finished, or manually if the slide
             * has no videos
             */
            WHEN_FINISHED,

            /**
             * Not yet supported! Continue after a timeout (specified separately).
             */
            TIMER
        }


        public signal void changed();

        /**
         * Emitted when a field is changed.
         */
        public signal void field_changed(string id, string val);


        internal signal void save_needed();


        public string layout_id { get; set; }

        public bool enabled { get; set; }

        public ContinueMode continue_mode { get; set; default=MANUAL; }
        public double continue_delay { get; set; default=10; }

        /**
         * The group that the slide is part of, if it is part of one.
         */
        public weak Group? group { get; internal set; }


        internal Json.Object json;


        public Slide(Json.Object json) {
            this.json = json;

            this.layout_id = this.json.get_string_member("layout");
            this.enabled = this.json.get_boolean_member("enabled");
            if (this.json.has_member("continue-mode")) {
                this.continue_mode = (ContinueMode) this.json.get_int_member("continue-mode");
            }
            if (this.json.has_member("continue-delay")) {
                this.continue_delay = this.json.get_double_member("continue-delay");
            }

            this.notify["layout-id"].connect((spec) => this.changed());
            this.notify["enabled"].connect((spec) => this.changed());
            this.notify["continue-mode"].connect((spec) => this.changed());
            this.notify["continue-delay"].connect((spec) => this.changed());

            this.changed.connect(() => {
                this.json.set_string_member("layout", this.layout_id);
                this.json.set_boolean_member("enabled", this.enabled);
                this.json.set_int_member("continue-mode", this.continue_mode);
                this.json.set_double_member("continue-delay", this.continue_delay);
                this.save();
            });
        }

        /**
         * Creates a new blank slide using the lyrics layout.
         */
        public Slide.blank() {
            this.for_lyrics("");
        }

        /**
         * Creates a new slide using the lyrics layout, with the given content.
         */
        public Slide.for_lyrics(string content) {
            var json = new Json.Object();
            json.set_boolean_member("enabled", true);
            json.set_string_member("layout", "lyrics");
            var fields = new Json.Object();
            fields.set_string_member("content", content);
            json.set_object_member("fields", fields);

            this(json);
        }

        /**
         * Creates a new slide using the media layout, showing the given media
         * resource.
         */
        public Slide.for_media(Resource resource) {
            var json = new Json.Object();
            json.set_boolean_member("enabled", true);
            json.set_string_member("layout", "media");
            var fields = new Json.Object();
            fields.set_string_member("media-content", resource.get_full_id());
            json.set_object_member("fields", fields);

            this(json);
        }

        public Slide.duplicate_of(Slide other) {
            Json.Object copy = Photon.duplicate_json(other.json);
            this(copy);
        }



        /**
         * Returns true if the slide uses this resource as a field.
         */
        public bool uses_resource(Resource resource) {
            bool result = false;
            this.json.get_object_member("fields").foreach_member(
                (obj, key, val) => {
                    if (val.get_string() == resource.get_full_id()) result = true;
                }
            );
            return result;
        }

        /**
         * Returns whether the slide is the first in its group, if it is in
         * a group.
         */
        public bool is_first_in_group() {
            if (this.group == null) return false;
            else return this.group.is_first_slide(this);
        }

        /**
         * Gets a field from the slide by its key.
         */
        public string? get_field(string key) {
            Json.Object fields = this.json.get_object_member("fields");
            if (fields.has_member(key)) return fields.get_string_member(key);
            else return null;
        }

        /**
         * Sets a field for this slide.
         *
         * Any key may be used, but the set of keys that will actually be used
         * is defined by the slide's layout.
         */
        public void set_field(string key, string? val) {
            Json.Object fields = this.json.get_object_member("fields");
            fields.set_string_member(key, val);

            this.field_changed(key, val);
            this.changed();
        }


        private void save() {
            this.save_needed();
        }
    }
}
