namespace Cellar.ImportUtils {
    public delegate void ResourceImported(Resource imported);

    public async void import_files(string[] files_original, ResourceImported cb) {
        // See https://gitlab.gnome.org/GNOME/vala/issues/751
        // You can't use a string array parameter after the first yield in an
        // async function, so we need to use a copy instead
        string[] files = files_original.copy();

        int files_done = 0;
        foreach (string uri in files) {
            File src = File.new_for_uri(uri);

            // Get resource type
            ResourceType type = get_import_type(uri);
            if (type == null) {
                warning("Cannot import %s because it is an unrecognized file type",
                        uri);
                continue;
            }

            // Create resource object
            Resource resource = new Resource.from_scratch(
                type,
                get_file_name(src),
                ""
            );

            // Ensure directory structure
            File dest = get_resource_file(resource.uuid);
            try {
                dest.get_parent().make_directory_with_parents();
            } catch (Error e) {
                // ignore, probably the directory already exists
            }

            // Thumbnails/subinfo
            if (type == ResourceType.VIDEO) {
                resource.subinfo = yield thumbnail_video(
                    uri,
                    get_thumbnail_file(resource.uuid).get_path()
                );
            }

            // Copy or link file
            try {
                if (type.is_media()) {
                    if (Photon.Config.get_instance().copy_on_import) {
                        yield src.copy_async(dest, FileCopyFlags.NONE);
                    } else {
                        dest.make_symbolic_link(dest.get_path());
                    }
                } else if (type.is_slideshow()) {
                    uint8[] contents;
                    src.load_contents(null, out contents, null);

                    Slideshow slideshow = parse_file((string) contents, resource);
                    yield Photon.save_json_now(dest, slideshow.json);
                }
            } catch (Error e) {
                warning("Could not import file '%s'! %s", uri, e.message);
            }

            // Done!
            cb(resource);
            files_done ++;
        }
    }

    public ResourceType? get_import_type(string uri) {
        string ext = uri.substring(uri.last_index_of(".") + 1);
        string type = ContentType.guess(uri, null, null);

        // Because they are plaintext files, song files aren't really detected
        // by mime type, so detect them by extension instead
        // .USR is used by SongSelect. .TXT files are assumed to simply be
        // lyrics pasted into a file.
        string[] songs = { "txt", "usr" };

        if (ContentType.is_mime_type(type, "image/*")) {
            return ResourceType.IMAGE;
        } else if (ContentType.is_mime_type(type, "video/*")) {
            return ResourceType.VIDEO;
        } else if (ext in songs) {
            return ResourceType.SONG;
        }

        return null;
    }

    /**
     * Returns true if the files can be imported as a single slideshow.
     */
    public bool can_import_as_slideshow(string[] uris) {
        // if there's only one slide, there's no reason to bother with a
        // slideshow resource
        if (uris.length <= 1) return false;

        foreach (var uri in uris) {
            ResourceType type = get_import_type(uri);
            if (type == null || !type.is_media()) return false;
        }

        return true;
    }

    /**
     * Gets a user-presentable name for the file.
     */
    public string get_file_name(File file) {
        string basename = file.get_basename();
        try {
            return /\.\w+/.replace(basename, basename.length, 0, "");
        } catch (RegexError e) {
            return basename;
        }
    }


    /**
     * Tries to find a name for a slideshow containing these files.
     *
     * Current implementation:
     * - If all the filenames start with the same 3+ characters, return those
     * characters
     * - Otherwise, return "New Slideshow" (localized)
     */
    public string get_common_name(string[] files) {
        string prefix = null;

        foreach (var filename in files) {
            File file = File.new_for_path(filename);
            string basename = file.get_basename();

            if (prefix == null) prefix = basename;
            else prefix = common_prefix(prefix, basename);

            if (prefix.length < 3) return _("New Slideshow");
        }

        // remove trailing underscores, dashes, whitespace
        try {
            prefix = /[-_\s]+$/.replace(prefix, prefix.length, 0, "");
        } catch(Error e) {
            // ignore
        }

        return prefix;
    }

    /**
     * Returns the longest string that is a prefix of both inputs.
     */
    private string common_prefix(string a, string b) {
        int len = 0;
        int a_l = a.char_count();
        int b_l = b.char_count();
        for (int i = 0; i < a_l && i < b_l; i ++) {
            if (a.get_char(i) != b.get_char(i)) break;
            len = i;
        }
        return a.substring(0, a.index_of_nth_char(len + 1));
    }
}
