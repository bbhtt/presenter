namespace Cellar {
    /**
     * Information about the slide order of a slideshow.
     *
     * There are two indexing systems used here: content and slide indexes.
     * A content index is an index in the list of groups and slides; that is,
     * a group only counts as one item in the content list. In the slide list,
     * a group counts for however many slides are in it.
     *
     * `cindex` is the standard variable name for a content index and `sindex`
     * is the name for a slide index.
     */
    public class Arrangement : Object {
        internal signal void save_needed();


        /**
         * Emitted when the slide list changes.
         */
        public signal void slides_spliced(int index, int added, int removed);


        /**
         * The #Slideshow this #Arrangement belongs to.
         */
        public weak Slideshow slideshow { get; private set; }


        /**
         * The slides in the arrangement.
         *
         * This list is always kept up to date with :content, which is
         * definitive.
         */
        private Gee.List<Slide> slides;

        /**
         * The content in the arrangement. This is the definitive source for
         * what slides are in the arrangement and in what order.
         */
        private Gee.List<ArrangementContent> contents;

        internal Json.Object json;


        /**
         * Creates a new Arrangement with the given UUID and JSON data.
         */
        internal Arrangement(Slideshow slideshow,
                             Json.Object json) {

            this.slideshow = slideshow;
            this.json = json;

            this.slideshow.slide_added.connect(this.on_slide_added_to_group);
            this.slideshow.slide_removed.connect(this.on_slide_removed_from_group);

            this.slides = new Gee.ArrayList<Slide>();
            this.contents = new Gee.ArrayList<ArrangementContent?>();

            this.json.get_array_member("contents").foreach_element(
                (array, index, val) => {
                    Json.Object obj = val.get_object();
                    if (obj.has_member("slide")) {
                        var slide = new Slide(obj.get_object_member("slide"));
                        var content = new ArrangementContent.for_slide(slide);
                        this.add_slide_internal(slide);
                        this.contents.add(content);

                        this.splice_slides(this.slides.size, { slide }, 0);
                    } else if (obj.has_member("group")) {
                        string group_uuid = obj.get_string_member("group");
                        Group group = this.slideshow.get_group(group_uuid);
                        var content = new ArrangementContent.for_group(group);
                        this.contents.add(content);

                        this.splice_slides(
                            this.slides.size, group.get_slides().to_array(), 0
                        );
                    }
                }
            );
        }

        /**
         * Creates a new Arrangement from scratch.
         */
        public Arrangement.from_scratch(Slideshow slideshow) {
            Json.Object json = new Json.Object();
            json.set_array_member("contents", new Json.Array());

            this(slideshow, json);
        }


        public Gee.List<Slide> get_slides() {
            return this.slides.read_only_view;
        }

        public Gee.List<ArrangementContent> get_contents() {
            return this.contents.read_only_view;
        }

        /**
         * Returns whether a slide can be inserted at the given position in
         * the arrangement.
         */
        public bool can_insert_at(int sindex) {
            return this.slide_to_content_index(sindex) != -1;
        }

        /**
         * Gets the content index containing the given slide index. Will return
         * return -1 if the slide is not at the beginning of its group (which
         * would make the mapping inexact).
         */
        public int slide_to_content_index(int sindex) {
            if (sindex == 0) return 0;
            if (sindex == -1) return this.contents.size;

            int i_s = 0;
            int i_c = 0;
            foreach (ArrangementContent content in this.contents) {
                if (content.slide != null) {
                    i_s ++;
                } else if (content.group != null) {
                    i_s += content.group.get_slides().size;
                }
                i_c ++;

                if (sindex == i_s) return i_c;
            }

            warning("Given slide index (%d) has no corresponding content index!".printf(sindex));
            return -1;
        }

        public int content_to_slide_index(int cindex) {
            if (cindex == 0) return 0;

            int i_s = 0;
            int i_c = 0;
            foreach (ArrangementContent content in this.contents) {
                if (content.slide != null) {
                    i_s ++;
                } else if (content.group != null) {
                    i_s += content.group.get_slides().size;
                }
                i_c ++;

                if (cindex == i_c) return i_s;
            }

            warning("Given content index (%d) is invalid!".printf(cindex));
            return -1;
        }

        /**
         * Inserts a slide into the arrangement. Will do nothing if the index
         * is in the middle of a group.
         */
        public void insert_slide(Slide slide, int sindex=-1) {
            int cindex;
            if (sindex == -1) {
                cindex = this.contents.size;
                sindex = this.content_to_slide_index(cindex);
            } else {
                cindex = this.slide_to_content_index(sindex);
            }
            if (cindex == -1) return;

            this.contents.insert(cindex, new ArrangementContent.for_slide(slide));
            this.add_slide_internal(slide);
            this.splice_slides(sindex, { slide }, 0);

            this.save();
        }

        /**
         * Inserts a group at the given content index.
         */
        public void insert_group(Group group, int cindex=-1) {
            if (cindex == -1) cindex = this.contents.size;

            this.contents.insert(cindex, new ArrangementContent.for_group(group));

            int sindex = this.content_to_slide_index(cindex);
            this.splice_slides(sindex, group.get_slides().to_array(), 0);
            this.save();
        }

        /**
         * Removes the slide or group at the given content index.
         */
        public void remove_content(int cindex) {
            int sindex = this.content_to_slide_index(cindex);

            if (sindex == -1) return;

            ArrangementContent content = this.contents.remove_at(cindex);
            if (content.group != null) {
                int removed = content.group.get_slides().size;
                this.splice_slides(sindex, null, removed);
            } else if (content.slide != null) {
                this.splice_slides(sindex, null, 1);
                this.remove_slide_internal(content.slide);
            }

            this.save();
        }

        /**
         * Removes the slide or group at the given content index.
         */
        public void remove_content_by_val(ArrangementContent val) {
            int cindex = this.contents.index_of(val);
            if (cindex != -1) this.remove_content(cindex);
        }

        /**
         * Removes any instance of the given group from the arrangement.
         */
        public void remove_group_by_value(Group group) {
            int[] to_remove = new int[0];
            int cindex = 0;
            foreach (ArrangementContent content in this.contents) {
                if (content.group == group) to_remove += cindex;
                cindex ++;
            }

            int removed_already = 0;
            foreach (int index in to_remove) {
                this.remove_content(index - removed_already);
                removed_already ++;
            }

            this.save();
        }


        /**
         * Should only be called for slides that are part of the arrangement
         * itself, not part of a group.
         */
        private void add_slide_internal(Slide slide) {
            slide.save_needed.connect(this.on_slide_save_needed);
        }
        private void remove_slide_internal(Slide slide) {
            slide.save_needed.disconnect(this.on_slide_save_needed);
        }
        /**
         * Note that this is only called when a slide that is directly part of
         * the arrangement needs saving, not for slides in groups.
         */
        private void on_slide_save_needed(Slide slide) {
            this.save();
        }

        private void on_slide_added_to_group(Group group, int index) {
            Slide slide = group.get_slides()[index];

            int c_index = 0;
            foreach (var content in this.contents) {
                if (content.group == group) {
                    int s_index = this.content_to_slide_index(c_index) + index;
                    this.splice_slides(s_index, { slide }, 0);
                }
                c_index ++;
            }
        }

        private void on_slide_removed_from_group(Group group, int index) {
            int c_index = 0;
            foreach (var content in this.contents) {
                if (content.group == group) {
                    int s_index = this.content_to_slide_index(c_index) + index;
                    this.splice_slides(s_index, null, 1);
                }
                c_index ++;
            }
        }

        /**
         * Syncs up the slides list with the content list by inserting and/or
         * removing some slides at a particular index.
         *
         * If no slides are added, @added should be null. If no slides are
         * removed, @removed should be 0.
         */
        private void splice_slides(int sindex, Slide[]? added, int removed) {
            // Remove slides
            for (int i = 0; i < removed; i ++) {
                this.slides.remove_at(sindex);
            }

            // Add slides
            if (added != null) {
                int num_added = 0;
                foreach (Slide slide in added) {
                    this.slides.insert(sindex + num_added, slide);
                    num_added ++;
                }
            }

            // Notify everyone else
            this.slides_spliced(sindex, added.length, removed);
        }

        /**
         * Makes any necessary changes to the JSON object, then emits
         * ::save_needed.
         */
        private void save() {
            Json.Array array = new Json.Array();
            foreach (ArrangementContent content in this.contents) {
                var obj = new Json.Object();
                if (content.group != null) {
                    obj.set_string_member("group", content.group.uuid);
                } else if (content.slide != null) {
                    obj.set_object_member("slide", content.slide.json);
                }
                array.add_object_element(obj);
            }
            this.json.set_array_member("contents", array);

            this.save_needed();
        }
    }


    /**
     * Either a slide or a group in an arrangement.
     */
    public class ArrangementContent {
        public Slide? slide { get; private set; }
        public Group? group { get; private set; }


        public ArrangementContent.for_slide(Slide slide) {
            this.slide = slide;
        }

        public ArrangementContent.for_group(Group group) {
            this.group = group;
        }
    }
}
