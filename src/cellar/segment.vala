namespace Cellar {
    /**
     * The type of segment (slideshow, header, or invalid).
     */
    public enum SegmentType {
        SLIDESHOW,
        HEADER,
        INVALID
    }

    /**
     * Represents a segment of a playlist.
     */
    public class Segment : Object {
        /**
         * Emitted when the segment's JSON data has changed and needs to be saved.
         * saved.
         */
        internal signal void save_needed();

        /**
         * Emitted when the segment changes. This includes when the slideshow's
         * slide arrangement changes.
         */
        public signal void changed();


        private string _name;
        /**
         * The user-defined name for the segment.
         */
        public string name {
            get {
                return this._name;
            }
            set {
                if (this.segment_type != HEADER) return;
                if (value.strip() == "") return;

                this._name = value;
                this.changed();
            }
        }

        /**
         * Whether the segment should loop
         */
        public bool looping { get; set; default=false; }

        /**
         * The type of segment that this is (slideshow, header, invalid).
         */
        public SegmentType segment_type { get; private set; }

        /**
         * The slideshow for this segment, if :segment_type is %SLIDESHOW.
         */
        public Slideshow? slideshow { get; private set; }

        /**
         * The media resource to use as a background for all slides in the
         * segment.
         */
        public Resource? background { get; set; }

        /**
         * A string that is displayed in the slides pane and stage view.
         */
        public string? comments { get; set; }


        internal Json.Object json;


        /**
         * Creates a new Segment with the given JSON data.
         */
        internal Segment(Json.Object json) {
            this.json = json;
            this._name = json.get_string_member("name");

            Library library = Library.get_instance();

            if (this.json.has_member("resource")) {
                // load slideshow from resource
                string uuid = this.json.get_string_member("resource");
                Resource res = library.get_resource(uuid);

                if (res == null) {
                    this.segment_type = INVALID;
                } else {
                    this._name = res.name;
                    this.json.set_string_member("name", res.name);
                    this.slideshow = res.slideshow;
                    this.segment_type = SLIDESHOW;
                }
            } else if (this.json.has_member("slideshow")) {
                // load slideshow from JSON
                Json.Object slides = this.json.get_object_member("slideshow");
                this.slideshow = new Slideshow(slides);
                this.slideshow.save_needed.connect(() => this.save_needed());
                this.segment_type = SLIDESHOW;
            } else {
                // header segment
                this.segment_type = SegmentType.HEADER;
            }

            if (this.json.has_member("looping")) {
                this.looping = this.json.get_boolean_member("looping");
            }

            if (this.json.has_member("background")) {
                string uuid = this.json.get_string_member("background");
                if (uuid != null) this.background = library.get_resource(uuid);
            }

            if (this.json.has_member("comments")) {
                this.comments = json.get_string_member("comments");
            }

            if (this.slideshow != null) {
                this.slideshow.arrangement.slides_spliced.connect(() => {
                    this.changed();
                });
            }

            this.notify["looping"].connect((spec) => this.changed());
            this.notify["background"].connect((spec) => {
                if (this.slideshow.resource != null) {
                    this.slideshow.resource.set_field("default-background",
                        this.background == null ? null : this.background.uuid
                    );
                }
                this.changed();
            });
            this.notify["comments"].connect((spec) => this.changed());

            this.changed.connect(() => {
                this.json.set_string_member("name", this.name);
                this.json.set_boolean_member("looping", this.looping);
                if (this.background != null) {
                    this.json.set_string_member("background", this.background.uuid);
                } else {
                    this.json.remove_member("background");
                }
                this.json.set_string_member("comments", this.comments);

                this.save_needed();
            });
        }

        /**
         * Creates a new slideshow segment.
         *
         * If the resource is media, a new internal slideshow will be created
         * for the segment, containing a single slide with the media resource.
         *
         * @resource: the resource to use
         */
        public Segment.from_resource(Resource resource) {
            var json = new Json.Object();
            json.set_string_member("name", resource.name);

            if (resource.resource_type.is_slideshow()) {
                json.set_string_member("resource", resource.uuid);

                string background = resource.get_field("default-background");
                if (background != null) json.set_string_member("background", background);
            } else {
                var slideshow = new Slideshow.from_media(resource);
                json.set_object_member("slideshow", slideshow.json);
            }

            this(json);
        }

        /**
         * Creates a new header segment.
         */
        public Segment.header(string name) {
            var json = new Json.Object();
            json.set_string_member("name", name);
            this(json);
        }


        /**
         * Returns true if the segment uses this resource somehow, like as a
         * background or slideshow.
         */
        public bool uses_resource(Resource resource) {
            if (this.background == resource) return true;
            if (this.slideshow != null && this.slideshow.resource == resource) return true;
            return false;
        }

        /**
         * Returns true if the segment is not a slideshow or if there are no
         * enabled slides in the segment.
         */
        public bool is_empty() {
            if (this.segment_type != SLIDESHOW) return true;
            foreach (Slide slide in this.slideshow.arrangement.get_slides()) {
                if (slide.enabled) return false;
            }
            return true;
        }

        /**
         * Adds the segment's slideshow to the Library as a resource, and
         * changes the segment to use the resource instead of the internal
         * version.
         */
        public void add_to_library() {
            return_if_fail(this.segment_type == SLIDESHOW);

            Cellar.Resource res = this.slideshow.add_to_library(this.name);

            if (this.json.has_member("slideshow")) {
                this.json.remove_member("slideshow");
            }
            this.json.set_string_member("resource", res.uuid);
            this.changed();
            this.save_needed();
        }
    }
}
