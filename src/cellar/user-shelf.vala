using Gee;

namespace Cellar {
    /**
     * A LibraryShelf that keeps track of the resources added by the user.
     */
    public class UserShelf : LibraryShelf {
        private HashMap<string, Resource> resources;

        private File file;
        private File dir;

        private Json.Object json;


        public UserShelf() {
            this.resources = new HashMap<string, Resource>();

            var config = Photon.Config.get_instance();
            this.file = File.new_for_path(config.datadir)
                        .get_child("library")
                        .get_child("index.json");
            this.dir = File.new_for_path(config.datadir)
                       .get_child("library")
                       .get_child("files");

            try {
                var parser = new Json.Parser();
                parser.load_from_file(this.file.get_path());
                this.json = parser.get_root().get_object();
            } catch (Error e) {
                debug("No user library found, starting anew");
                this.json = new Json.Object();
                this.save();
            }

            this.json.foreach_member((obj, key, val) => {
                var resource = new Resource(key, val.get_object());
                this.add_internal(resource);
            });
        }


        public override Resource? get_resource(string uuid) {
            return this.resources.get(uuid);
        }

        public override Collection<Resource> get_all_resources() {
            return this.resources.values;
        }

        public override bool remove_resource(string uuid) {
            if (this.resources.has_key(uuid)) {
                Resource res = this.resources.get(uuid);

                res.delete_files();

                this.json.remove_member(uuid);
                this.remove_internal(uuid);
                this.resource_removed(res);
                this.save();
                return true;
            }
            return false;
        }

        public void add_resource(Resource resource) {
            if (this.json.has_member(resource.uuid)) {
                warning("Cannot add the same resource to a shelf twice!");
                debug("Resource UUID: %s", resource.uuid);
                return;
            }

            this.json.set_object_member(resource.uuid, resource.json);
            this.add_internal(resource);
            this.resource_added(resource);

            this.save();
        }

        private void import_files_slideshow(string[] files) {
            var slideshow = new Slideshow.from_scratch();
            var slideshow_res = slideshow.add_to_library(
                ImportUtils.get_common_name(files), false
            );

            ImportUtils.import_files.begin(files, (resource) => {
                slideshow_res.add_subresource(resource);
                var slide = new Slide.for_media(resource);
                slideshow.arrangement.insert_slide(slide);
            }, (obj, res) => {
                ImportUtils.import_files.end(res);
                this.save();
            });
        }

        public void import_files(string[] files, bool as_slideshow) {
            if (as_slideshow) {
                this.import_files_slideshow(files);
                return;
            }

            ImportUtils.import_files.begin(files, (resource) => {
                this.add_resource(resource);
            }, (obj, res) => {
                ImportUtils.import_files.end(res);
                this.save();
            });
        }

        public ResourceType? get_import_type(string file) {
            return ImportUtils.get_import_type(file);
        }


        /**
         * Adds the Resource to the in-memory list.
         *
         * This is used internally to maintain `resources`. It is not for
         * adding a new resource to the library, see add_resource().
         */
        private void add_internal(Resource resource) {
            this.resources[resource.uuid] = resource;
            resource.changed.connect(this.on_resource_changed);
            resource.save_needed.connect(this.save);
            resource.subresource_added.connect(this.on_subresource_added);
            resource.subresource_removed.connect(this.on_subresource_removed);
        }

        /**
         * Removes a Resource by UUID from the in-memory list.
         */
        private void remove_internal(string uuid) {
            Resource res;
            this.resources.unset(uuid, out res);
            if (res != null) {
                res.save_needed.disconnect(this.save);
                res.changed.disconnect(this.on_resource_changed);
                res.subresource_added.disconnect(this.on_subresource_added);
                res.subresource_removed.disconnect(this.on_subresource_removed);
            }
        }

        /**
         * Signal handler for resources' ::changed signals.
         */
        private void on_resource_changed(Resource resource, Resource origin) {
            this.resource_changed(origin);
        }

        /**
         * Signal handler for resources' ::subresource_added signals.
         */
        private void on_subresource_added(Resource resource, Resource origin) {
            this.resource_added(origin);
        }

        /**
         * Signal handler for resources' ::subresource_removed signals.
         */
        private void on_subresource_removed(Resource resource, Resource origin) {
            this.resource_removed(origin);
        }

        /**
         * Saves the resource index to disk.
         */
        private void save() {
            Photon.save_json(this.file, this.json);
        }
    }
}
