namespace Cellar {
    private const double THUMBNAIL_SEEK_POS = 0.1;

    public async string thumbnail_video(string src_uri, string destfile) {
        SourceFunc callback = thumbnail_video.callback;

        string subinfo = "";
        ThreadFunc<bool> func = () => {
            Gst.Pipeline pipeline = new Gst.Pipeline(null);

            Gst.Element videoconvert = Gst.ElementFactory.make("videoconvert", null);

            dynamic Gst.Element uridecodebin = Gst.ElementFactory.make("uridecodebin", null);
            uridecodebin.pad_added.connect((src, new_pad) => {
                if (new_pad.is_linked()) return;

                Gst.Caps caps = new_pad.get_current_caps();
                unowned Gst.Structure struct = caps.get_structure(0);
                string pad_type = struct.get_name();
                if (!pad_type.has_prefix("video/x-raw")) return;

                Gst.Pad sink = videoconvert.get_static_pad("sink");
                new_pad.link(sink);
            });
            uridecodebin.uri = src_uri;

            int64 duration = 0;
            bool terminate = false, seek_started = false;

            Gst.App.Sink appsink = (Gst.App.Sink) Gst.ElementFactory.make("appsink", null);
            appsink.caps = new Gst.Caps.simple(
                "video/x-raw",
                "format", Type.STRING, BYTE_ORDER == LITTLE_ENDIAN ? "BGRA" : "RGBA"
            );

            pipeline.add_many(uridecodebin, videoconvert, appsink);
            uridecodebin.link(videoconvert);
            videoconvert.link(appsink);

            pipeline.set_state(PAUSED);
            pipeline.get_state(null, null, Gst.CLOCK_TIME_NONE);

            Gst.Bus bus = pipeline.get_bus();

            while (!terminate) {
                Gst.Message msg = bus.timed_pop_filtered(
                    Gst.CLOCK_TIME_NONE,
                    ERROR | EOS | DURATION_CHANGED | ASYNC_DONE
                );

                switch (msg.type) {
                    case Gst.MessageType.EOS:
                        terminate = true;
                        break;
                    case Gst.MessageType.ERROR:
                        Error err;
                        string debug_info;
                        msg.parse_error(out err, out debug_info);
                        warning("Error generating thumbnail for video! %s", err.message);
                        debug("Debug info for error: %s", debug_info);
                        terminate = true;
                        break;
                    case Gst.MessageType.ASYNC_DONE:
                        if (!seek_started) {
                            // get duration
                            bool success = pipeline.query_duration(Gst.Format.TIME, out duration);
                            if (!success) break;

                            // seek a bit of the way in to the video to get a thumbnail
                            seek_started = true;
                            pipeline.seek_simple(
                                Gst.Format.TIME,
                                FLUSH | KEY_UNIT,
                                (int64) (duration * THUMBNAIL_SEEK_POS)
                            );
                        } else {
                            pipeline.set_state(PLAYING);

                            Gst.Sample sample = appsink.pull_sample();

                            unowned Gst.Structure s = sample.get_caps().get_structure(0);
                            int width, height;
                            s.get_int("width", out width);
                            s.get_int("height", out height);
                            var surf = new Cairo.ImageSurface(Cairo.Format.ARGB32, width, height);

                            Gst.Buffer buf = sample.get_buffer();
                            Photon.copy_buffer_to_surface(buf, surf);
                            surf.write_to_png(destfile);

                            terminate = true;
                        }
                        break;
                }
            }

            pipeline.set_state(NULL);

            subinfo = Photon.seconds_to_string((int) (duration / 1000000000));
            Idle.add((owned) callback);
            return true;
        };
        new Thread<bool>(null, (owned) func);

        yield;

        return subinfo;
    }
}
