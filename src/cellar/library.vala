using Gee;

namespace Cellar {
    /*
     * Represents the user's Library. Keeps track of all the resources in it.
     */
    public class Library : Object {
        private static Library instance;
        public static Library get_instance() {
            if (instance == null) instance = new Library();
            return instance;
        }

        /**
         * Emitted when a resource has been added to the library.
         */
        public signal void resource_added(Resource resource);

        /**
         * Emitted when a resource's basic info has changed.
         */
        public signal void resource_changed(Resource resource);

        /**
         * Emitted when a resource is removed.
         */
        public signal void resource_removed(Resource resource);


        /**
         * The shelf that stores resources added by the user.
         */
        public UserShelf user_shelf { get; private set; }


        /**
         * All shelves in the library, including `user_shelf`.
         */
        private LibraryShelf[] shelves;

        /**
         * A list of all resources in the library. Compiled from the various
         * shelves and kept up to date using signal handlers.
         */
        private Gee.List<Resource> resources;


        private Library() {
            this.resources = new LinkedList<Resource>();

            this.shelves = new LibraryShelf[0];

            this.user_shelf = new UserShelf();
            this.add_shelf_internal(this.user_shelf);
        }


        /**
         * Gets a Resource object for the given resource UUID.
         */
        public Resource? get_resource(string id) {
            string path = null;
            string uuid = null;
            if ("/" in id) {
                uuid = id.split("/", 2)[0];
                path = id.split("/", 2)[1];
            } else {
                uuid = id;
            }

            Resource res = null;
            foreach (var shelf in this.shelves) {
                res = shelf.get_resource(uuid);
                if (res != null) break;
            }

            if (res != null && path != null) {
                foreach (var segment in path.split("/")) {
                    res = res.get_subresource(segment);
                    if (res == null) break;
                }
            }

            return res;
        }

        /**
         * Removes a resource from whatever shelf it is on.
         */
        public void remove_resource(string uuid) {
            foreach (var shelf in this.shelves) {
                if (shelf.remove_resource(uuid)) return;
            }
        }

        /**
         * Returns a list of all resources on all shelves.
         */
        public Gee.List<Resource> get_all_resources() {
            return this.resources.read_only_view;
        }


        /**
         * Adds a shelf to the library.
         */
        private void add_shelf_internal(LibraryShelf shelf) {
            shelf.resource_added.connect(this.shelf_resource_added);
            shelf.resource_changed.connect(this.shelf_resource_changed);
            shelf.resource_removed.connect(this.shelf_resource_removed);
            this.shelves += shelf;
            this.resources.add_all(shelf.get_all_resources());
        }

        /**
         * Signal handler for when a resource is added to a shelf
         */
        private void shelf_resource_added(LibraryShelf shelf,
                                          Resource resource) {
            this.resources.add(resource);
            this.resource_added(resource);
        }
        /**
         * Signal handler for when a resource on a shelf is changed
         */
        private void shelf_resource_changed(LibraryShelf shelf,
                                            Resource resource) {
            this.resource_changed(resource);
        }
        /**
         * Signal handler for when a resource is removed from a shelf
         */
        private void shelf_resource_removed(LibraryShelf shelf,
                                            Resource resource) {
            this.resource_removed(resource);
            this.resources.remove(resource);
        }
    }
}
