using Gee;

namespace Cellar {
    /**
     * Base class for Library "shelves", which provide the resources listed in
     * the library.
     *
     * The primary one is the UserShelf, which keeps track of the resources
     * the user has added. However, there may be others, such as a shelf
     * providing resources for connected webcams.
     */
    public abstract class LibraryShelf : Object {
        /**
         * Emitted when a resource has been added to the shelf.
         */
        public signal void resource_added(Resource resource);

        /**
         * Emitted when a resource has been removed from the shelf.
         */
        public signal void resource_removed(Resource resource);

        /**
         * Emitted when the basic information about a resource has changed.
         *
         * This is limited to things like name, subinfo, and timestamp.
         */
        public signal void resource_changed(Resource resource);


        /**
         * Gets a resource, if it is on this shelf.
         */
        public abstract Resource? get_resource(string uuid);

        /**
         * Gets a list of all resources on the shelf.
         */
         public abstract Collection<Resource> get_all_resources();

         /**
          * Removes a resource, if it is on this shelf.
          * Returns: true if a resource was removed
          */
         public abstract bool remove_resource(string uuid);
    }
}
