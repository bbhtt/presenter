namespace Photon {
    public extern const string BUILD_COMMIT;
    public extern const string BUILD_VERSION;

    /**
     * True if the executable is being built as part of a packaged installation.
     */
    public extern const bool IS_PACKAGED;

    /**
     * The OS that Floodlight is being run on. This is determined by Meson at
     * compile time.
     */
    public extern const string OS;
    public const string MACOS = "darwin";
    public const string LINUX = "linux";
    public const string WINDOWS = "windows";
}
