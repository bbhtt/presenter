/* config-file.vala
 *
 * Copyright 2020 James Westman <james@flyingpimonster.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

public class Photon.ConfigFile : Object {
    private File file;
    private Json.Object json;


    public ConfigFile(string path) {
        File file = File.new_for_path(Photon.Config.get_instance().datadir)
                    .get_child(path);
        this.from_file(file);
    }

    public ConfigFile.from_file(File file) {
        this.file = file;

        try {
            var parser = new Json.Parser();
            parser.load_from_file(file.get_path());
            this.json = parser.get_root().get_object();
        } catch (Error e) {
            message("Creating file: %s", file.get_path());
            this.json = new Json.Object();
        }
    }


    public void set_int(string key, int val, string? section=null) {
        this.get_section(section, true).set_int_member(key, val);
    }

    public int get_int(string key, int default_val=0, string? section=null) {
        var obj = this.get_section(section);
        if (obj == null || !obj.has_member(key)) {
            return default_val;
        }

        return (int) obj.get_int_member(key);
    }

    public void set_int64(string key, int64 val, string? section=null) {
        this.get_section(section, true).set_int_member(key, val);
    }

    public int64 get_int64(string key, int64 default_val=0, string? section=null) {
        var obj = this.get_section(section);
        if (obj == null || !obj.has_member(key)) {
            return default_val;
        }

        return obj.get_int_member(key);
    }

    public void set_double(string key, double val, string? section=null) {
        this.get_section(section, true).set_double_member(key, val);
    }

    public double get_double(string key, double default_val=0, string? section=null) {
        var obj = this.get_section(section);
        if (obj == null || !obj.has_member(key)) {
            return default_val;
        }

        return obj.get_double_member(key);
    }

    public void set_bool(string key, bool val, string? section=null) {
        this.get_section(section, true).set_boolean_member(key, val);
    }

    public bool get_bool(string key, bool default_val=false, string? section=null) {
        var obj = this.get_section(section);
        if (obj == null || !obj.has_member(key)) {
            return default_val;
        }

        return obj.get_boolean_member(key);
    }

    public void set_string(string key, string val, string? section=null) {
        this.get_section(section, true).set_string_member(key, val);
    }

    public string get_string(string key, string? default_val=null, string? section=null) {
        var obj = this.get_section(section);
        if (obj == null || !obj.has_member(key)) {
            return default_val;
        }

        return obj.get_string_member(key);
    }


    public void save() {
        save_json(this.file, this.json);
    }


    private Json.Object get_section(string? section, bool create=false) {
        if (section == null) {
            return this.json;
        } else if (this.json.has_member(section)) {
            return this.json.get_object_member(section);
        } else {
            var obj = new Json.Object();
            this.json.set_object_member(section, obj);
            return obj;
        }
    }
}
