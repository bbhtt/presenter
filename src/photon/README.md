# Photon: Core Floodlight Systems

Photon is the module at the bottom of the Floodlight stack. It handles the
basics, like finding and reading the config file. It also contains a number of
common utility functions and classes.
