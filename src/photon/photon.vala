namespace Photon {
    [CCode (cname="GETTEXT_PACKAGE")]
    internal extern const string GETTEXT_PACKAGE;
    [CCode (cname="LOCALEDIR")]
    internal extern const string LOCALEDIR;

    #if PHOTON_IS_MACOS
        [CCode]
        extern string macos_get_data_dir();
    #endif

    [CCode]
    extern unowned void copy_buffer_to_surface(Gst.Buffer buf, Cairo.ImageSurface surf);

    internal static bool initted = false;
    public void init() {
        if (initted) return;

        Intl.setlocale (LocaleCategory.ALL, "");
        Intl.bindtextdomain (GETTEXT_PACKAGE, LOCALEDIR);
        Intl.bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
        Intl.textdomain (GETTEXT_PACKAGE);

        initted = true;
    }


    /**
     * The UUID of the playlist that is created when the program is run for the
     * first time
     */
    public const string DEFAULT_PLAYLIST = "71003f85-a00e-4539-a42f-abba1d8ecf6c";
}
