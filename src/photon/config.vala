namespace Photon {
    /**
     * Loads and stores configuration options for Floodlight.
     */
    public class Config : Object {
        private static Config instance;
        public static Config get_instance() {
            if (instance == null) instance = new Config();
            return instance;
        }


        public bool dark_theme { get; set; }

        public int framerate { get; set; }
        public double crossfade_duration { get; set; }

        public double thumbnail_aspect { get; set; }
        public int thumbnail_width { get; set; }

        public double volume { get; set; }
        public bool volume_mute { get; set; }

        public bool copy_on_import { get; set; }
        public int import_lines_per_slide { get; set; }
        public bool show_import_suggestions { get; set; }

        public bool debugmode { get; set; }

        public bool remotes_enabled { get; set; }

        public bool check_for_updates { get; set; }

        public string datadir { get; private set; }

        /*
         * The file to save the config to
         */
        private File file;

        private ConfigFile config;


        /*
         * Creates a new Config object from the default config file.
         */
        public Config() {
            File file = File.new_for_path(Environment.get_user_data_dir())
                        .get_child("Floodlight")
                        .get_child("presenter-data")
                        .get_child("config")
                        .get_child("config.json");

            this.config = new ConfigFile.from_file(file);

            this.dark_theme = config.get_bool("dark_theme", true);
            this.framerate = config.get_int("framerate", 60);
            this.crossfade_duration = config.get_double("crossfade_duration", 0.25);
            this.thumbnail_aspect = config.get_double("thumbnail_aspect", 16.0/9);
            this.thumbnail_width = config.get_int("thumbnail_width", 160);
            this.volume = config.get_double("volume", 100);
            this.volume_mute = config.get_bool("volume_mute", false);
            this.copy_on_import = config.get_bool("copy_on_import", true);
            this.import_lines_per_slide = config.get_int("import_lines_per_slide", 4);
            this.show_import_suggestions = config.get_bool("show_import_suggestions", true);
            this.debugmode = config.get_bool("debugmode", false);
            this.remotes_enabled = config.get_bool("remotes_enabled", false);
            this.check_for_updates = config.get_bool("check_for_updates", true);

            this.file = file;
            this.datadir = file.get_parent().get_parent().get_path();

            this.debug_dump();


            this.notify.connect((spec) => {
                this.save();
            });
        }


        /*
         * Loads options from a JSON file.
         */
        private void load(File file) throws Error {
        }

        /*
         * Writes all config values to stdout.
         */
        private void debug_dump() {
            print("\x1B[33mConfiguration (%s)\x1B[0m\n", this.file.get_path());
            print("\x1B[33mdark_theme                   %d\x1B[0m\n", (int) this.dark_theme);
            print("\x1B[33mframerate                    %d\x1B[0m\n", this.framerate);
            print("\x1B[33mcrossfade_duration           %f\x1B[0m\n", this.crossfade_duration);
            print("\x1B[33mthumbnail_aspect             %f\x1B[0m\n", this.thumbnail_aspect);
            print("\x1B[33mthumbnail_width              %d\x1B[0m\n", this.thumbnail_width);
            print("\x1B[33mvolume                       %f\x1B[0m\n", this.volume);
            print("\x1B[33mvolume_mute                  %s\x1B[0m\n", this.volume_mute ? "true" : "false");
            print("\x1B[33mcopy_on_import               %s\x1B[0m\n", this.copy_on_import ? "true" : "false");
            print("\x1B[33mimport_lines_per_slide       %d\x1B[0m\n", this.import_lines_per_slide);
            print("\x1B[33mshow_import_suggestions      %s\x1B[0m\n", this.show_import_suggestions ? "true" : "false");
            print("\x1B[33mdebugmode                    %s\x1B[0m\n", this.debugmode ? "true" : "false");
            print("\x1B[33mcheck_for_updates            %s\x1B[0m\n", this.check_for_updates ? "true" : "false");
            print("\x1B[33mremotes_enabled              %s\x1B[0m\n", this.remotes_enabled ? "true" : "false");
            print("\n");
        }

        /*
         * Saves the config back to its file
         */
        private void save() {
            this.config.set_bool("dark_theme", this.dark_theme);
            this.config.set_int("framerate", this.framerate);
            this.config.set_double("crossfade_duration", this.crossfade_duration);
            this.config.set_double("thumbnail_aspect", this.thumbnail_aspect);
            this.config.set_int("thumbnail_width", this.thumbnail_width);
            this.config.set_double("volume", this.volume);
            this.config.set_bool("volume_mute", this.volume_mute);
            this.config.set_bool("copy_on_import", this.copy_on_import);
            this.config.set_int("import_lines_per_slide", this.import_lines_per_slide);
            this.config.set_bool("show_import_suggestions", this.show_import_suggestions);
            this.config.set_bool("debugmode", this.debugmode);
            this.config.set_bool("check_for_updates", this.check_for_updates);
            this.config.set_bool("remotes_enabled", this.remotes_enabled);

            this.config.save();
        }
    }
}
