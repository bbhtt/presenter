namespace Glitter {
    [GtkTemplate (ui="/io/gitlab/floodlight/Presenter/ui/presenter-console.ui")]
    public class PresenterConsole : Gtk.ApplicationWindow {
        private static PresenterConsole instance;
        public static PresenterConsole get_instance() {
            if (instance == null) {
                critical("Something went horribly wrong in the code! No PresenterConsole has been created yet!");
            }
            return instance;
        }


        [GtkChild] public unowned BottomTabs tabs;
        [GtkChild] public unowned Gtk.Paned left_split;
        [GtkChild] public unowned Gtk.Paned right_split;
        [GtkChild] public unowned Gtk.Paned main_split;
        [GtkChild] public unowned HeaderBar headerbar;
        [GtkChild] public unowned HeaderBar fake_headerbar;
        [GtkChild] public unowned OutputView output;
        [GtkChild] public unowned SlidesView slides;
        [GtkChild] public unowned LibraryView library_view;
        [GtkChild] public unowned Gtk.Stack playlist_revealer_icons;
        [GtkChild] public unowned Gtk.Stack library_revealer_icons;
        [GtkChild] public unowned Gtk.Revealer playlist_revealer;
        [GtkChild] public unowned Gtk.Revealer library_revealer;


        private bool is_fullscreen = false;


        public PresenterConsole(Gtk.Application app) {
            Object(application: app);

            if (instance != null) {
                critical("Something went horribly wrong in the code! A PresenterConsole has already been created!");
                return;
            }
            instance = this;

            if (Photon.OS != Photon.MACOS) {
                // Remove the fake headerbar, which is only used on macOS
                this.fake_headerbar.parent.remove(this.fake_headerbar);
            }

            // Thumbnail class needs to be initialized
            Thumbnails.get_instance();

            // Set dark theme according to config
            var config = Photon.Config.get_instance();
            var gtk_settings = Gtk.Settings.get_default();
            config.bind_property(
                "dark-theme", gtk_settings, "gtk-application-prefer-dark-theme",
                DEFAULT | SYNC_CREATE
            );

            var css = new Gtk.CssProvider();
            css.load_from_resource("/io/gitlab/floodlight/Presenter/ui/styles.css");
            // see https://developer.gnome.org/gtk3/unstable/GtkStyleProvider.html#GTK-STYLE-PROVIDER-PRIORITY-APPLICATION:CAPS
            Gtk.StyleContext.add_provider_for_screen(Gdk.Screen.get_default(), css, 600);
            if (Photon.OS == Photon.MACOS) {
                var css_macos = new Gtk.CssProvider();
                css_macos.load_from_resource("/io/gitlab/floodlight/Presenter/ui/styles-macos.css");
                Gtk.StyleContext.add_provider_for_screen(Gdk.Screen.get_default(), css_macos, 600);
            }
            try {
                var css_groups = new Gtk.CssProvider();
                css_groups.load_from_data(GroupColors.get_instance().get_css());
                Gtk.StyleContext.add_provider_for_screen(Gdk.Screen.get_default(), css_groups, 600);
            } catch (Error e) {
                warning("Could not load CSS styles for group labels!");
            }

            var display = Gdk.Display.get_default();
            int monitors = display.get_n_monitors();
            for (int i = 0; i < monitors; i ++) {
                var monitor = display.get_monitor(i);
                if (monitor.is_primary()) {
                    this.move(monitor.geometry.x, monitor.geometry.y);
                    break;
                }
            }

            UIState.restore();
        }


        public void toggle_fullscreen() {
            if (this.is_fullscreen) this.unfullscreen();
            else this.fullscreen();
        }

        public void set_show_playlist(bool show) {
            if (show) {
                this.playlist_revealer.reveal_child = true;
                this.playlist_revealer.vexpand = true;
                this.playlist_revealer_icons.set_visible_child_name("expanded");
            } else {
                this.playlist_revealer.reveal_child = false;
                this.playlist_revealer.vexpand = false;
                this.playlist_revealer_icons.set_visible_child_name("collapsed");
            }
        }

        public void set_show_library(bool show) {
            if (show) {
                this.library_revealer.reveal_child = true;
                this.library_revealer.vexpand = true;
                this.library_revealer_icons.set_visible_child_name("expanded");
            } else {
                this.library_revealer.reveal_child = false;
                this.library_revealer.vexpand = false;
                this.library_revealer_icons.set_visible_child_name("collapsed");
            }
        }


        [GtkCallback]
        private bool on_key_press_event(Gdk.EventKey event) {
            // If a text entry is focused, we want the keypress to go there instead
            // of advancing the slide
            Gtk.Widget focused = this.get_focus();
            if (!(focused is Gtk.Entry) && !(focused is Gtk.TextView)) {
                if (event.keyval == Gdk.Key.Right || event.keyval == Gdk.Key.Down) {
                    Halogen.State.get_instance().next();
                } else if (event.keyval == Gdk.Key.Left || event.keyval == Gdk.Key.Up) {
                    Halogen.State.get_instance().prev();
                }
                return true;
            }

            return false;
        }

        [GtkCallback]
        private bool on_window_state_event(Gdk.EventWindowState event) {
            this.is_fullscreen = Gdk.WindowState.FULLSCREEN in event.new_window_state;
            this.fake_headerbar.visible = this.is_fullscreen;
            this.fake_headerbar.set_fullscreen(this.is_fullscreen);
            this.headerbar.set_fullscreen(this.is_fullscreen);

            return false;
        }

        [GtkCallback]
        private bool on_toggle_playlist_revealer(Gdk.EventButton event) {
            if (event.button == Gdk.BUTTON_PRIMARY) {
                this.set_show_playlist(!this.playlist_revealer.reveal_child);
            }
            return true;
        }

        [GtkCallback]
        private bool on_toggle_library_revealer(Gdk.EventButton event) {
            if (event.button == Gdk.BUTTON_PRIMARY) {
                this.set_show_library(!this.library_revealer.reveal_child);
            }
            return true;
        }

        [GtkCallback]
        private bool on_delete_event() {
            UIState.save();
            Halogen.DisplayManager.get_instance().hide();
            return false;
        }
    }
}
