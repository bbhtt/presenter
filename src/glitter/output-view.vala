namespace Glitter {
    [GtkTemplate (ui="/io/gitlab/floodlight/Presenter/ui/output-view.ui")]
    public class OutputView : Gtk.Stack {
        [GtkChild] private unowned Gtk.Notebook notebook;
        [GtkChild] private unowned Gtk.Stack play_pause;
        [GtkChild] private unowned Gtk.Label duration_label;
        [GtkChild] private unowned Gtk.Box duration_bar;
        [GtkChild] private unowned Gtk.Label duration_tooltip;
        [GtkChild] private unowned Gtk.Adjustment seek_adjustment;

        private bool seeking = false;

        construct {
            Halogen.DisplayManager.get_instance().changed.connect(this.create_tabs);
            Halogen.State.get_instance().slide_changed.connect(this.on_slide_changed);
            this.create_tabs();

            Timeout.add(200, () => {
                // cancel timeout when stuff starts getting torn down on quit
                if (this.seek_adjustment == null) return Source.REMOVE;

                var pool = Halogen.State.get_instance().pool;
                if (pool != null) {
                    double pos = pool.get_position();
                    double left = pool.duration - pos;
                    if (!this.seeking) this.seek_adjustment.value = pos;
                    this.duration_label.label = Photon.seconds_to_string((int) left);

                    this.duration_tooltip.label =
                        @"$(Photon.seconds_to_string((int) pos))/$(Photon.seconds_to_string((int) pool.duration))";
                }
                return Source.CONTINUE;
            }, Priority.LOW);
        }


        private void create_tabs() {
            while (this.notebook.get_n_pages() > 0) this.notebook.remove_page(0);

            var displays = Halogen.DisplayManager.get_instance();
            int page_count = displays.get_physical_display_count();
            bool empty = true;
            for (int i = 0; i < page_count; i ++) {
                Halogen.Display display = displays.get_display(i);

                // only show tabs for displays that are plugged in and enabled
                if (display.window != null && display.feed_id != null) {
                    var feeds = Halogen.FeedManager.get_instance();
                    string name = feeds.get_feeds().@get(display.feed_id);
                    var label = new Gtk.Label(name);
                    label.show();

                    var tab = new OutputTab(display);
                    tab.show();

                    this.notebook.append_page(tab, label);
                    empty = false;
                }
            }

            this.visible_child_name = empty ? "empty" : "nonempty";
        }


        private void on_slide_changed() {
            this.play_pause.set_visible_child_name("pause");
            var pool = Halogen.State.get_instance().pool;
            pool.notify["duration"].connect(this.update_seek_bar);
            this.update_seek_bar();
            this.seeking = false;
            this.seek_adjustment.value = 0;
        }

        private void update_seek_bar() {
            var pool = Halogen.State.get_instance().pool;
            if (pool == null) this.duration_bar.visible = false;

            this.duration_label.label = Photon.seconds_to_string((int) pool.duration);
            this.seek_adjustment.upper = pool.duration;

            this.duration_bar.visible = (pool.duration > 0);
        }

        [GtkCallback]
        private void on_display_settings_clicked() {
            PresenterConsole.get_instance().tabs.show_tab(new TabInfo(PREFERENCES, null, "displays"));
        }

        [GtkCallback]
        private void on_pause_clicked() {
            var pool = Halogen.State.get_instance().pool;
            pool.pause(!pool.paused);
            this.play_pause.set_visible_child_name(pool.paused ? "play" : "pause");
        }

        [GtkCallback]
        private void on_seek() {
            // This function is called every time the slider changes; check
            // this.seeking to see if the user is currently dragging it
            if (this.seeking) {
                debug("Seeking: %f", this.seek_adjustment.value);
                var pool = Halogen.State.get_instance().pool;
                pool.seek(this.seek_adjustment.value);
            }
        }

        [GtkCallback]
        private bool on_seek_button_down(Gdk.EventButton event) {
            this.seeking = true;
            return false;
        }
        [GtkCallback]
        private bool on_seek_button_up(Gdk.EventButton event) {
            this.seeking = false;
            return false;
        }

        [GtkCallback]
        private bool on_duration_label_query_tooltip(int x, int y, bool keyboard, Gtk.Tooltip tooltip) {
            tooltip.set_custom(this.duration_tooltip);
            return true;
        }
    }


    public class OutputTab : Gtk.DrawingArea {
        private Halogen.Display display;


        public OutputTab(Halogen.Display display) {
            this.display = display;

            this.display.window.buffer.swapped.connect(() => {
                Idle.add(() => {
                    this.queue_draw();
                    return Source.REMOVE;
                });
            });

            this.draw.connect(this.on_draw);
        }


        private bool on_draw(Cairo.Context cr) {
            Gtk.Allocation alloc;
            this.get_allocation(out alloc);

            cr.set_source_rgb(0.5, 0.5, 0.5);
            cr.paint();

            if (this.display.window != null) {
                this.display.window.buffer.with_front((front) => {
                    Halogen.draw_to_context(
                        cr, front,
                        0, 0, alloc.width, alloc.height
                    );
                });
            }

            return false;
        }
    }
}
