namespace Glitter {
    [GtkTemplate (ui="/io/gitlab/floodlight/Presenter/ui/headerbar/headerbar.ui")]
    public class HeaderBar : Gtk.HeaderBar {
        // [GtkChild] private unowned Gtk.Widget window_controls;
        [GtkChild] private unowned Gtk.Widget window_controls_close;
        // [GtkChild] private unowned Gtk.Widget window_controls_mac;
        // [GtkChild] private unowned Gtk.Widget window_controls_mac_close;
        // [GtkChild] private unowned Gtk.Widget window_controls_mac_minimize;
        [GtkChild] private unowned Gtk.Popover quit_popover;
        [GtkChild] private unowned Gtk.Stack fullscreen_icons;
        [GtkChild] private unowned Blinker output_blinker;
        [GtkChild] private unowned Gtk.Switch output_switch;
        [GtkChild] public unowned PlaylistSwitcher playlist_switcher;
        [GtkChild] private unowned Gtk.Adjustment volume_adjustment;
        [GtkChild] private unowned Gtk.ToggleButton volume_mute;
        [GtkChild] private unowned Gtk.Stack volume_icon_stack;
        [GtkChild] private unowned Gtk.Label volume_label;
        [GtkChild] private unowned Blinker volume_blinker;
        [GtkChild] public unowned ImportSuggestions import_suggestions;
        [GtkChild] private unowned Gtk.Button updates;


        construct {
            // if (Photon.OS == Photon.MACOS) {
            //     this.window_controls.hide();
            //     this.quit_popover.relative_to = this.window_controls_mac_close;
            //     this.window_controls_mac.show();
            // } else {
            //     this.quit_popover.relative_to = this.window_controls_close;
            // }
            this.quit_popover.relative_to = this.window_controls_close;

            Halogen.DisplayManager.get_instance().notify["hidden"].connect(
                this.on_displays_hidden_changed
            );
            this.on_displays_hidden_changed();

            var config = Photon.Config.get_instance();
            config.bind_property(
                "volume", this.volume_adjustment, "value", SYNC_CREATE | BIDIRECTIONAL
            );
            config.bind_property(
                "volume-mute", this.volume_mute, "active", SYNC_CREATE | BIDIRECTIONAL
            );
            config.notify["volume"].connect(this.update_volume_label);
            config.notify["volume-mute"].connect(this.update_volume_label);
            Halogen.DisplayManager.get_instance().notify["hidden"].connect(this.update_volume_label);
            this.update_volume_label();

            var updates = Photon.UpdateCheck.get_instance();
            updates.bind_property("update-available", this.updates, "visible", SYNC_CREATE);
        }


        /**
         * Called by the window to set which icon should be shown on the
         * fullscreen button.
         */
        public void set_fullscreen(bool fullscreen) {
            this.fullscreen_icons.visible_child_name = fullscreen ? "unfullscreen" : "fullscreen";
        }


        [GtkCallback]
        private void on_quit_clicked(Gtk.Button button) {
            PresenterConsole.get_instance().close();
        }

        [GtkCallback]
        private void on_minimize_clicked(Gtk.Button button) {
            PresenterConsole.get_instance().iconify();
        }

        [GtkCallback]
        private void on_fullscreen_clicked(Gtk.Button button) {
            PresenterConsole.get_instance().toggle_fullscreen();
        }

        [GtkCallback]
        private void on_cancel_quit_clicked(Gtk.Button button) {
            this.quit_popover.popdown();
        }

        private void on_displays_hidden_changed() {
            this.output_switch.state = !Halogen.DisplayManager.get_instance().hidden;
        }

        [GtkCallback]
        private bool on_output_toggled(Gtk.Switch widget, bool on) {
            var displays = Halogen.DisplayManager.get_instance();
            if (on) {
                displays.show();
                output_blinker.active = false;
            } else {
                displays.hide();
                output_blinker.active = true;
            }
            return false;
        }

        [GtkCallback]
        private void on_updates_clicked(Gtk.Button button) {
            var info = new Glitter.TabInfo(UPDATES, null);
            Glitter.PresenterConsole.get_instance().tabs.show_tab(info);
            this.updates.get_style_context().remove_class("suggested-action");
        }

        private void update_volume_label() {
            double volume = Photon.Config.get_instance().volume;

            if (Halogen.DisplayManager.get_instance().hidden) {
                this.volume_label.label = _("OFF");
                this.volume_blinker.active = true;
                this.volume_icon_stack.visible_child_name = "mute";
            } else if (Photon.Config.get_instance().volume_mute || volume == 0) {
                this.volume_label.label = _("MUTE");
                this.volume_blinker.active = true;
                this.volume_icon_stack.visible_child_name = "mute";
            } else {
                this.volume_label.label = ((int) volume).to_string() + "%";
                this.volume_blinker.active = false;

                if (volume < 50) this.volume_icon_stack.visible_child_name = "low";
                else if (volume < 100) this.volume_icon_stack.visible_child_name = "high";
                else this.volume_icon_stack.visible_child_name = "full";
            }
        }
    }
}
