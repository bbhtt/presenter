namespace Glitter {
    [GtkTemplate (ui="/io/gitlab/floodlight/Presenter/ui/headerbar/import-suggestions.ui")]
    public class ImportSuggestions : Gtk.MenuButton {
        [GtkChild] private unowned Gtk.ListBox listbox;


        construct {
            ImportSuggestionsMonitor.get_instance().file_added.connect(this.on_file_added);
            ImportSuggestionsMonitor.get_instance().file_removed.connect(this.on_file_removed);

            Photon.Config.get_instance().notify["show-import-suggestions"].connect(() => {
                this.update_visibility();
            });
        }


        private void update_visibility() {
            if (!Photon.Config.get_instance().show_import_suggestions ||
                this.listbox.get_children().length() == 0) {

                this.hide();
            } else {
                this.show();
            }
        }

        private void on_file_added(File file) {
            this.listbox.add(new ImportSuggestionsRow(file));

            // indicate that a new file has been discovered
            this.update_visibility();
            this.get_style_context().add_class("suggested-action");
        }

        private void on_file_removed(File file) {
            List<weak Gtk.Widget> children = this.listbox.get_children();
            foreach (var row in children) {
                if (((ImportSuggestionsRow) row).file.equal(file)) {
                    this.listbox.remove(row);
                }
            };

            this.update_visibility();
        }

        [GtkCallback]
        private void on_clicked() {
            // User has opened the menu, no need to draw their attention
            // anymore
            this.get_style_context().remove_class("suggested-action");
        }

        [GtkCallback]
        private void on_clear_all_clicked() {
            ImportSuggestionsMonitor.get_instance().ignore_all();
        }
    }


    /**
     * Monitors the Downloads folder for new importable files.
     */
    public class ImportSuggestionsMonitor : Object {
        private static ImportSuggestionsMonitor instance;
        public static ImportSuggestionsMonitor get_instance() {
            if (instance == null) instance = new ImportSuggestionsMonitor();
            return instance;
        }


        /**
         * Emitted when a new suggested file is found.
         */
        public signal void file_added(File file);

        /**
         * Emitted when a file is removed from the list (either imported,
         * ignored, or deleted).
         */
        public signal void file_removed(File file);


        /**
         * A set of files the user might want to import.
         */
        public Gee.Set<File> suggestions;


        /**
         * The user's Downloads directory, which we are monitoring.
         */
        private File downloads;

        /**
         * The monitor that is monitoring the Downloads directory.
         */
        private FileMonitor monitor;

        /**
         * A set of files that have been ignored by the user.
         */
        private Gee.Set<File> ignored;


        private File file;
        private Json.Object json;


        private ImportSuggestionsMonitor() {
            this.suggestions = make_file_set();
            this.ignored = make_file_set();


            // Load file
            var config = Photon.Config.get_instance();
            this.file = File.new_for_path(config.datadir)
                        .get_child("import-suggestions.json");

            try {
                var parser = new Json.Parser();
                parser.load_from_file(this.file.get_path());
                this.json = parser.get_root().get_object();
            } catch (Error e) {
                debug("No import suggestion file found, starting anew");
                this.json = new Json.Object();
            }

            if (!this.json.has_member("ignored")) {
                this.json.set_array_member("ignored", new Json.Array());
            }
            this.json.get_array_member("ignored").foreach_element((array, index, val) => {
                this.ignored.add(File.new_for_path(val.get_string()));
            });

            // Set up monitor
            string path = Environment.get_user_special_dir(DOWNLOAD);
            this.downloads = File.new_for_path(path);

            try {
                this.monitor = this.downloads.monitor_directory(WATCH_MOVES);

                this.monitor.changed.connect((file, other, event) => {
                    switch (event) {
                        case CREATED:
                        case MOVED_IN:
                            this.add_new_file(file);
                            break;
                        case DELETED:
                        case MOVED_OUT:
                            this.remove_deleted_file(file);
                            break;
                        case RENAMED:
                            this.remove_deleted_file(file);
                            this.add_new_file(other);
                            break;
                    }
                });
            } catch (Error e) {
                warning("Cannot create file monitor for import suggestions! %s", e.message);
            }

            // Go through existing files
            this.enumerate_files.begin();
        }


        /**
         * Adds a file to the "ignore" list, so that it is not suggested
         * anymore.
         */
        public void ignore_suggestion(File file) {
            this.suggestions.remove(file);
            this.ignored.add(file);
            this.file_removed(file);
            this.save();
        }

        /**
         * Marks all current suggestions as ignored.
         */
        public void ignore_all() {
            File[] copy = this.suggestions.to_array();
            foreach (File file in copy) {
                this.ignore_suggestion(file);
            }
        }


        /**
         * Called on startup to sync the internal lists with the files that
         * actually exist in the Downloads folder.
         *
         * This function first makes a list of the files in the directory.
         * Then, it removes from the ignored list any files that no longer
         * exist.
         * Lastly, it adds all files not on the ignored list to the suggestions
         * list.
         */
        private async void enumerate_files() {
            var files = make_file_set();
            var user_shelf = Cellar.Library.get_instance().user_shelf;

            try {
                FileEnumerator enumerator = yield this.downloads.enumerate_children_async("standard::*", NOFOLLOW_SYMLINKS);
                while (true) {
                    FileInfo? info = enumerator.next_file();
                    if (info == null) break;

                    File file = this.downloads.get_child(info.get_name());

                    // add the file only if it is importable
                    if (user_shelf.get_import_type(file.get_uri()) != null) {
                        files.add(file);
                    }
                }
            } catch (Error e) {
                warning("Error listing files in Downloads directory! %s", e.message);
            }

            // If a file does not exist, it should not be in the ignored list
            bool changed = this.ignored.retain_all(files);
            if (changed) this.save();

            // Files on the ignored list should not be suggested
            files.remove_all(this.ignored);
            this.suggestions.add_all(files);
            foreach (var file in files) this.file_added(file);
        }

        /**
         * Called when a file is discovered, to add it to the list if it is
         * importable.
         */
        private void add_new_file(File file) {
            var user_shelf = Cellar.Library.get_instance().user_shelf;
            if (user_shelf.get_import_type(file.get_uri()) != null) {
                this.suggestions.add(file);
                this.file_added(file);
            }
        }

        /**
         * Removes from both lists a file that has been deleted/moved out of
         * the directory.
         */
        private void remove_deleted_file(File file) {
            bool changed = this.suggestions.remove(file);
            if (changed) this.file_removed(file);

            changed = this.ignored.remove(file);
            if (changed) this.save();
        }

        private void save() {
            var ignored = new Json.Array();
            foreach (var file in this.ignored) {
                ignored.add_string_element(file.get_path());
            }
            this.json.set_array_member("ignored", ignored);

            Photon.save_json(this.file, this.json);
        }
    }


    private Gee.Set<File> make_file_set() {
        return new Gee.HashSet<File>((file) => file.hash(), (a, b) => a.equal(b));
    }
}
