namespace Glitter {
    [GtkTemplate (ui="/io/gitlab/floodlight/Presenter/ui/headerbar/import-suggestions-row.ui")]
    public class ImportSuggestionsRow : Gtk.ListBoxRow {
        public File file;


        [GtkChild] private unowned Gtk.Label name_label;
        [GtkChild] private unowned Gtk.Image icon;


        public ImportSuggestionsRow(File file) {
            this.file = file;

            this.name_label.label = this.file.get_basename();
            this.name_label.tooltip_text = this.file.get_basename();
            var user_shelf = Cellar.Library.get_instance().user_shelf;

            Cellar.ResourceType type = user_shelf.get_import_type(this.file.get_uri());
            this.icon.icon_name = type.icon;
            this.icon.get_style_context().add_class("resource-icon-" + type.id);
        }


        [GtkCallback]
        private void on_import_clicked() {
            var user_shelf = Cellar.Library.get_instance().user_shelf;
            user_shelf.import_files({ this.file.get_uri() }, false);
            ImportSuggestionsMonitor.get_instance().ignore_suggestion(this.file);
        }

        [GtkCallback]
        private void on_ignore_clicked() {
            ImportSuggestionsMonitor.get_instance().ignore_suggestion(this.file);
        }
    }
}
