namespace Glitter {
    [GtkTemplate (ui="/io/gitlab/floodlight/Presenter/ui/tabs/slideshow-editor/slide-list.ui")]
    public class SlideList : Gtk.Box {
        private Cellar.Arrangement _arrangement;
        public Cellar.Arrangement arrangement {
            get {
                return _arrangement;
            }
            set {
                if (_arrangement != null) {
                    _arrangement.slides_spliced.disconnect(this.on_slides_spliced);
                }

                _arrangement = value;
                _arrangement.slides_spliced.connect(this.on_slides_spliced);
                this.rebuild_ui();
            }
        }


        [GtkChild] private unowned Gtk.ListBox slide_list;
        [GtkChild] private unowned SlideEditor slide_editor;


        construct {
            this.slide_list.set_header_func((current, before) => {
                SlideListRow row = (SlideListRow) current;
                if (row.slide.is_first_in_group()) {
                    Gtk.Label header = new Gtk.Label(row.slide.group.name);
                    header.halign = Gtk.Align.START;
                    header.get_style_context().add_class("bold");
                    row.set_header(header);
                } else {
                    row.set_header(null);
                }
            });
        }


        private void rebuild_ui() {
            clear_widget(this.slide_list);
            foreach (var slide in this.arrangement.get_slides()) {
                var row = new SlideListRow(slide);
                row.add_slide.connect(this.on_add_slide_requested);
                row.merge_slide.connect(this.on_slide_merge_requested);
                this.slide_list.add(row);
            }
        }

        /**
         * Emitted when a slide row has requested that a slide be added.
         */
        private void on_add_slide_requested(SlideListRow row,
                                            Cellar.Slide new_slide,
                                            int pos) {
            if (pos != 0 && pos != 1) {
                warning("pos must be either 0 or 1!");
                return;
            }

            if (row.slide.group == null) {
                // add to arrangement
                int index = row.get_index();
                this.arrangement.insert_slide(new_slide, index + pos);
            } else {
                // add to group
                int index = row.slide.group.index_of(row.slide);
                row.slide.group.insert_slide(new_slide, index + pos);
            }
        }

        /**
         * Called when a slide should be merged into the one above it, if
         * possible. This happens when you press backspace with the cursor
         * already at the beginning of the text view (you are "deleting" the
         * slide break).
         *
         * Does nothing if merging the slides is impossible.
         */
        private void on_slide_merge_requested(SlideListRow row) {
            int index = row.get_index();

            if (index == 0) return;

            Cellar.Slide target = this.arrangement.get_slides()[index - 1];
            Cellar.Slide slide = this.arrangement.get_slides()[index];

            if (target.group != slide.group) return;

            if (target.layout_id != slide.layout_id) return;
            if (target.layout_id != "lyrics") return;

            // append the slide's content to the target's
            string target_content = target.get_field("content");
            string slide_content = slide.get_field("content");
            if (target_content != null) {
                target.set_field("content", target_content + "\n" + slide_content);
            } else {
                target.set_field("content", slide_content);
            }

            // remove the original slide
            if (slide.group != null) {
                slide.group.remove_slide(slide);
            } else {
                int cindex = this.arrangement.slide_to_content_index(index);
                this.arrangement.remove_content(cindex);
            }
        }

        private void on_slides_spliced(int index, int added, int removed) {
            this.rebuild_ui();

            if (added > 0) {
                Gtk.ListBoxRow row = this.slide_list.get_row_at_index(index);
                this.slide_list.select_row(row);
            }
        }

        [GtkCallback]
        private void on_slide_selected(Gtk.ListBox box, Gtk.ListBoxRow? selected) {
            if (selected == null) {
                this.slide_editor.slide = null;
                return;
            }

            SlideListRow row = (SlideListRow) selected;
            this.slide_editor.slide = row.slide;
        }
    }
}
