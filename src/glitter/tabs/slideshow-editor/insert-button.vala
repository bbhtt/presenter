namespace Glitter {
    /**
     * A label representing a group or slide in the rearranger.
     */
    [GtkTemplate (ui="/io/gitlab/floodlight/Presenter/ui/tabs/slideshow-editor/insert-button.ui")]
    public class InsertButton : Gtk.MenuButton {
        public Cellar.Arrangement arrangement { get; private set; }
        public int content_index { get; private set; }


        [GtkChild] private unowned Gtk.Box box;


        public InsertButton(Cellar.Arrangement arrangement, int content_index) {
            this.arrangement = arrangement;
            this.content_index = content_index;

            this.arrangement.slideshow.groups_changed.connect(this.rebuild_ui);
            this.rebuild_ui();
        }


        public void add_group(Cellar.Group group) {
            this.arrangement.insert_group(group, this.content_index);
        }

        public void create_group() {
            Cellar.Group group = this.arrangement.slideshow.add_group();
            this.add_group(group);
        }

        public void add_slide() {
            int s_index = this.arrangement.content_to_slide_index(this.content_index);
            if (s_index == -1) {
                // could happen if arrangement is empty; add slide to beginning
                s_index = 0;
            }

            var slide = new Cellar.Slide.blank();
            this.arrangement.insert_slide(slide, s_index);
        }


        private void rebuild_ui() {
            clear_widget(this.box);

            var groups = this.arrangement.slideshow.get_groups();

            // Alphabetize the list of groups
            var groups_list = new Gee.LinkedList<Cellar.Group>();
            groups_list.add_all(groups);
            groups_list.sort((a, b) => strcmp(a.name, b.name));

            foreach (Cellar.Group group in groups_list) {
                this.box.add(new InsertButtonGroup(this, group));
            }
            if (groups.size > 0) {
                var separator = new Gtk.Separator(Gtk.Orientation.HORIZONTAL);
                separator.visible = true;
                this.box.add(separator);
            }

            this.box.add(new InsertButtonGroup(this, null));
            this.box.add(new InsertButtonGroup(this, null, true));
        }
    }


    /**
     * A button in the InsertButton popover, to add a group to the arrangement.
     */
    public class InsertButtonGroup : Gtk.Button {
        public Cellar.Group group;


        private weak InsertButton button;

        /**
         * Whether a blank slide will be inserted rather than a group.
         */
        private bool slide;


        /**
         * Creates a new button to insert the given group. If `group` is null,
         * a new group will be created when the button is clicked. If `slide`
         * is true, a blank slide will be inserted instead.
         */
        public InsertButtonGroup(InsertButton button, Cellar.Group? group, bool slide=false) {
            this.button = button;
            this.group = group;
            this.slide = slide;

            Gtk.StyleContext style = this.get_style_context();
            style.add_class("menuitem");
            style.add_class("flat");
            style.add_class("button");
            style.add_class("small-button");

            if (this.slide) {
                this.label = _("New Slide\u2026");
            } else if (this.group != null) {
                this.group.bind_property("name", this, "label", SYNC_CREATE);
                GroupColors.get_instance().bind_label_style(this, this.group);
            } else {
                this.label = _("New Group\u2026");
            }

            this.visible = true;

            this.clicked.connect(this.on_clicked);
        }


        private void on_clicked() {
            if (this.slide) {
                this.button.add_slide();
            } else if (this.group != null) {
                this.button.add_group(this.group);
            } else {
                this.button.create_group();
            }
        }
    }
}
