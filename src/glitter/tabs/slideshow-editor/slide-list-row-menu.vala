namespace Glitter {
    [GtkTemplate (ui="/io/gitlab/floodlight/Presenter/ui/tabs/slideshow-editor/slide-list-row-menu.ui")]
    public class SlideListRowMenu : Gtk.Menu {
        public weak SlideListRow slide_list_row;


        [GtkCallback]
        private void on_delete_item_activated() {
            this.slide_list_row.on_delete_item_activated();
        }

        [GtkCallback]
        private void on_new_slide_before_item_activated() {
            this.slide_list_row.on_new_slide_before_item_activated();
        }

        [GtkCallback]
        private void on_new_slide_after_item_activated() {
            this.slide_list_row.on_new_slide_after_item_activated();
        }

        [GtkCallback]
        private void on_rename_group_item_activated() {
            this.slide_list_row.on_rename_group_item_activated();
        }
    }
}
