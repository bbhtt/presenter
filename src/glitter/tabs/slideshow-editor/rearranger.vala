namespace Glitter {
    [GtkTemplate (ui="/io/gitlab/floodlight/Presenter/ui/tabs/slideshow-editor/rearranger.ui")]
    public class Rearranger : Gtk.Box {
        private Cellar.Arrangement _arrangement;
        /**
         * The arrangement being rearranged.
         */
        public Cellar.Arrangement arrangement {
            get {
                return this._arrangement;
            }
            set {
                if (value == null) return;

                if (_arrangement != null) {
                    _arrangement.slides_spliced.disconnect(this.on_arrangement_spliced);
                }

                _arrangement = value;
                _arrangement.slides_spliced.connect(this.on_arrangement_spliced);
                this.rebuild_ui();
            }
        }


        construct {
        }


        private void rebuild_ui() {
            clear_widget(this);

            int i = 0;
            for (int n = this.arrangement.get_contents().size; i < n; i ++) {
                this.add(new InsertButton(this.arrangement, i));
                this.add(new GroupLabel(this.arrangement, i));
            }
            this.add(new InsertButton(this.arrangement, i));
        }


        private void on_arrangement_spliced(Cellar.Arrangement arrangement,
                                            int index, int added, int removed) {
            this.rebuild_ui();
        }
     }
}
