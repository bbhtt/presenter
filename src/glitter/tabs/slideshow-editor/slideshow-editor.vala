namespace Glitter {
    [GtkTemplate (ui="/io/gitlab/floodlight/Presenter/ui/tabs/slideshow-editor/slideshow-editor.ui")]
    public class SlideshowEditorTab : Gtk.Box, Tab {
        public string label { get; protected set; default="..."; }
        public TabInfo tab_info { get; protected set; }


        [GtkChild] private unowned Rearranger rearranger;
        [GtkChild] private unowned SlideList slide_list;


        private Cellar.Resource resource;


        public SlideshowEditorTab(TabInfo tab_info) {
            this.tab_info = tab_info;
            this.resource = Cellar.Library.get_instance().get_resource(tab_info.path);
            if (this.resource == null) return;

            var arrangement = this.resource.slideshow.arrangement;
            this.rearranger.arrangement = arrangement;
            this.slide_list.arrangement = arrangement;

            this.resource.bind_property("name", this, "label", SYNC_CREATE, (binding, from, ref to) => {
                to = _("Edit %s").printf((string) from);
                return true;
            });

            this.resource.removed.connect(this.close);
        }
    }
}
