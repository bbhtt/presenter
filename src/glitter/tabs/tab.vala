namespace Glitter {
    public interface Tab : Gtk.Widget {
        /**
         * The TabInfo describing the tab.
         */
        public abstract TabInfo tab_info { get; protected set; }

        /**
         * The label for the tab.
         */
        public abstract string label { get; protected set; }

        /**
         * Closes the tab.
         */
        public void close() {
            PresenterConsole.get_instance().tabs.close_tab(this);
        }
    }
}
