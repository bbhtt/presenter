namespace Glitter {
    [GtkTemplate (ui="/io/gitlab/floodlight/Presenter/ui/tabs/about.ui")]
    public class AboutTab : Gtk.Box, Tab {
        public string label { get; protected set; default=_("About"); }
        public TabInfo tab_info { get; protected set; }


        [GtkChild] private unowned Gtk.Label version_label;


        public AboutTab(TabInfo tab_info) {
            this.tab_info = tab_info;

            this.version_label.label = "%s (%s)".printf(
                Photon.BUILD_VERSION, Photon.BUILD_COMMIT
            );
        }


        [GtkCallback]
        private void on_view_site_clicked() {
            try {
                Gtk.show_uri_on_window(null, "https://floodlight.gitlab.io", Gdk.CURRENT_TIME);
            } catch (Error e) {
                // not much we can do
            }
        }
    }
}
