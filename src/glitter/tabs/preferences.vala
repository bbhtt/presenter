namespace Glitter {
    [GtkTemplate (ui="/io/gitlab/floodlight/Presenter/ui/tabs/preferences.ui")]
    public class PreferencesTab : Gtk.Box, Tab {
        public string label { get; protected set; default=_("Preferences"); }
        public TabInfo tab_info { get; protected set; }


        [GtkChild] private unowned Gtk.Adjustment thumbnail_size;
        [GtkChild] private unowned Gtk.ComboBoxText thumbnail_aspect;
        [GtkChild] private unowned Gtk.Switch dark_theme;
        [GtkChild] private unowned Gtk.Switch check_for_updates;
        [GtkChild] private unowned Gtk.Switch suggest_from_downloads;
        [GtkChild] private unowned Gtk.SpinButton lines_per_slide;
        [GtkChild] private unowned Gtk.ListBox display_list;
        [GtkChild] private unowned Gtk.ListStore feed_store;
        [GtkChild] private unowned Gtk.Stack stack;

        private Photon.Config config;

        public PreferencesTab(TabInfo tab_info) {
            this.tab_info = tab_info;
            this.config = Photon.Config.get_instance();

            tab_info.bind_property("subpath", this.stack, "visible-child-name", SYNC_CREATE | BIDIRECTIONAL);

            config.bind_property(
                "dark-theme", this.dark_theme, "active",
                SYNC_CREATE | BIDIRECTIONAL
            );
            config.bind_property(
                "thumbnail-width", this.thumbnail_size, "value",
                SYNC_CREATE | BIDIRECTIONAL
            );
            if (this.config.thumbnail_aspect == 16/9.0) {
                this.thumbnail_aspect.active_id = "16:9";
            } else {
                this.thumbnail_aspect.active_id = "4:3";
            }
            config.bind_property(
                "check-for-updates", this.check_for_updates, "active",
                SYNC_CREATE | BIDIRECTIONAL
            );

            config.bind_property(
                "show-import-suggestions", this.suggest_from_downloads, "active",
                SYNC_CREATE | BIDIRECTIONAL
            );
            config.bind_property(
                "import-lines-per-slide", this.lines_per_slide, "value",
                SYNC_CREATE | BIDIRECTIONAL
            );

            var feeds = Halogen.FeedManager.get_instance();
            foreach (Gee.Map.Entry<string, string> entry in feeds.get_feeds().entries) {
                this.feed_store.insert_with_values(null, -1,
                    0, entry.key,
                    1, entry.value
                );
            }

            this.create_displays_list();
            Halogen.DisplayManager.get_instance().physical_displays_changed
                    .connect(this.create_displays_list);
        }


        [GtkCallback]
        private void on_thumbnail_aspect_changed() {
            if (this.thumbnail_aspect.active_id == "16:9") {
                this.config.thumbnail_aspect = 16/9.0;
            } else {
                this.config.thumbnail_aspect = 4/3.0;
            }
        }


        /**
         * Populates `display_list` with DisplayListRow widgets.
         */
        private void create_displays_list() {
            clear_widget(this.display_list);

            int monitors = Gdk.Display.get_default().get_n_monitors();
            for (int i = 0; i < monitors; i ++) {
                this.display_list.add(new DisplayListRow(i, this.feed_store));
            }
        }
    }
}
