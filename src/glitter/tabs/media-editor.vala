namespace Glitter {
    [GtkTemplate (ui="/io/gitlab/floodlight/Presenter/ui/tabs/media-editor.ui")]
    public class MediaEditorTab : Gtk.Box, Tab {
        public string label { get; protected set; }
        public TabInfo tab_info { get; protected set; }


        [GtkChild] private unowned Gtk.Image thumbnail;
        [GtkChild] private unowned Gtk.Label video_label;
        [GtkChild] private unowned Gtk.Grid video_controls;
        [GtkChild] private unowned Gtk.RadioButton scale_fit;
        [GtkChild] private unowned Gtk.RadioButton scale_fill;
        [GtkChild] private unowned Gtk.RadioButton scale_stretch;
        [GtkChild] private unowned Gtk.RadioButton scale_shrink;
        [GtkChild] private unowned Gtk.Label rotate_label;
        [GtkChild] private unowned Gtk.ToggleButton hflip;
        [GtkChild] private unowned Gtk.ToggleButton vflip;
        [GtkChild] private unowned Gtk.RadioButton end_cut;
        [GtkChild] private unowned Gtk.RadioButton end_freeze;
        [GtkChild] private unowned Gtk.RadioButton end_loop;
        [GtkChild] private unowned Gtk.Adjustment volume_adjustment;
        [GtkChild] private unowned Gtk.Stack preset_chooser_stack;


        private Cellar.Resource resource;


        public MediaEditorTab(TabInfo tab_info) {
            this.tab_info = tab_info;

            this.resource = Cellar.Library.get_instance().get_resource(tab_info.path);
            if (this.resource == null) return;

            this.resource.bind_property("name", this, "label", SYNC_CREATE, (binding, from, ref to) => {
                to = _("Edit %s").printf((string) from);
                return true;
            });

            if (this.resource.resource_type != Cellar.ResourceType.VIDEO) {
                this.video_label.hide();
                this.video_controls.hide();
            }

            switch ((Halogen.ScaleOpts) int.parse(this.resource.get_field("scale", "0"))) {
                case FIT:
                    this.scale_fit.active = true;
                    break;
                case FILL:
                    this.scale_fill.active = true;
                    break;
                case STRETCH:
                    this.scale_stretch.active = true;
                    break;
                case SHRINK:
                    this.scale_shrink.active = true;
                    break;
            }

            this.set_rotate_label();

            this.hflip.active = this.resource.get_field("hflip") == "true";
            this.vflip.active = this.resource.get_field("vflip") == "true";

            switch (this.resource.get_field("end-mode", "cut")) {
                case "cut":
                    this.end_cut.active = true;
                    break;
                case "freeze":
                    this.end_freeze.active = true;
                    break;
                case "loop":
                    this.end_loop.active = true;
                    break;
            }

            var volume = double.parse(this.resource.get_field("volume", "100"));
            this.volume_adjustment.@value = volume;

            this.update_thumbnail();

            if (this.resource.get_field("preset-chosen") == null) {
                this.preset_chooser_stack.visible_child_name = "presets";
            } else {
                this.preset_chooser_stack.visible_child_name = "normal";
            }

            this.resource.removed.connect(this.close);
        }


        private void set_rotate_label() {
            this.rotate_label.label = this.resource.get_field("rotate", "0") + "\u00B0";
        }

        [GtkCallback]
        private void on_set_scale_fit() {
            if (this.scale_fit.active) {
                this.resource.set_field("scale", ((int) Halogen.ScaleOpts.FIT).to_string());
                this.update_thumbnail();
            }
        }

        [GtkCallback]
        private void on_set_scale_fill() {
            if (this.scale_fill.active) {
                this.resource.set_field("scale", ((int) Halogen.ScaleOpts.FILL).to_string());
                this.update_thumbnail();
            }
        }

        [GtkCallback]
        private void on_set_scale_stretch() {
            if (this.scale_stretch.active) {
                this.resource.set_field("scale", ((int) Halogen.ScaleOpts.STRETCH).to_string());
                this.update_thumbnail();
            }
        }

        [GtkCallback]
        private void on_set_scale_shrink() {
            if (this.scale_shrink.active) {
                this.resource.set_field("scale", ((int) Halogen.ScaleOpts.SHRINK).to_string());
                this.update_thumbnail();
            }
        }

        [GtkCallback]
        private void on_rotate_cw() {
            int rotate = int.parse(this.resource.get_field("rotate", "0"));
            rotate += 90;
            if (rotate >= 360) rotate -= 360;
            this.resource.set_field("rotate", rotate.to_string());
            this.set_rotate_label();
            this.update_thumbnail();
        }

        [GtkCallback]
        private void on_rotate_ccw() {
            int rotate = int.parse(this.resource.get_field("rotate", "0"));
            rotate -= 90;
            if (rotate >= 360) rotate -= 360;
            this.resource.set_field("rotate", rotate.to_string());
            this.set_rotate_label();
            this.update_thumbnail();
        }

        [GtkCallback]
        private void on_hflip_toggled() {
            this.resource.set_field("hflip", this.hflip.active ? "true" : "false");
            this.update_thumbnail();
        }

        [GtkCallback]
        private void on_set_end_cut() {
            if (this.end_cut.active) {
                this.resource.set_field("end-mode", "cut");
                this.update_thumbnail();
            }
        }

        [GtkCallback]
        private void on_set_end_freeze() {
            if (this.end_freeze.active) {
                this.resource.set_field("end-mode", "freeze");
                this.update_thumbnail();
            }
        }

        [GtkCallback]
        private void on_set_end_loop() {
            if (this.end_loop.active) {
                this.resource.set_field("end-mode", "loop");
                this.update_thumbnail();
            }
        }

        [GtkCallback]
        private void on_vflip_toggled() {
            this.resource.set_field("vflip", this.vflip.active ? "true" : "false");
            this.update_thumbnail();
        }

        [GtkCallback]
        private void on_volume_changed() {
            this.resource.set_field("volume", this.volume_adjustment.@value.to_string());
        }

        [GtkCallback]
        private void on_preset_background_chosen() {
            this.scale_fill.active = true;
            this.volume_adjustment.value = 0;
            this.end_loop.active = true;
            this.resource.set_field("preset-chosen", "true");

            this.preset_chooser_stack.visible_child_name = "normal";
            this.update_thumbnail();
        }

        [GtkCallback]
        private void on_preset_media_chosen() {
            this.scale_fit.active = true;
            this.resource.set_field("preset-chosen", "true");

            this.preset_chooser_stack.visible_child_name = "normal";
            this.update_thumbnail();
        }

        private void update_thumbnail() {
            Thumbnails.thumbnail_resource(this.thumbnail, this.resource);
        }
    }
}
