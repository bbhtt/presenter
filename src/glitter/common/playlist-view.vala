namespace Glitter {
    [GtkTemplate (ui="/io/gitlab/floodlight/Presenter/ui/common/playlist-view.ui")]
    public class PlaylistView : Gtk.ScrolledWindow {
        [GtkChild] private unowned Gtk.ListBox listbox;


        private Cellar.Playlist _playlist;
        /**
         * The playlist that is being viewed. If `preview` is enabled, this
         * is set manually. Otherwise, it will be synced to the currently open
         * playlist.
         */
        public Cellar.Playlist? playlist {
            get {
                return _playlist;
            }
            set {
                if (_playlist != value) {
                    if (_playlist != null) {
                        _playlist.segment_added.disconnect(this.on_segment_added);
                        _playlist.segment_removed.disconnect(this.on_segment_removed);
                        _playlist.segment_moved.disconnect(this.on_segment_moved);
                    }

                    _playlist = value;

                    if (_playlist != null) {
                        _playlist.segment_added.connect(this.on_segment_added);
                        _playlist.segment_removed.connect(this.on_segment_removed);
                        _playlist.segment_moved.connect(this.on_segment_moved);
                    }

                    this.rebuild_ui();
                }
            }
        }


        public bool preview { get; set construct; default=false; }


        construct {
            if (!this.preview) {
                Halogen.State.get_instance().notify["playlist"].connect(() => {
                    this.playlist = Halogen.State.get_instance().playlist;
                });
                this.playlist = Halogen.State.get_instance().playlist;

                Halogen.State.get_instance().notify["segment"].connect(() => {
                    this.select_correct_row();
                });

                Gtk.drag_dest_set(
                    this.listbox,
                    Gtk.DestDefaults.ALL,
                    { get_drag_target_resources(), get_drag_target_segment() },
                    Gdk.DragAction.COPY
                );
            }
        }


        /**
         * Rebuilds the UI from scratch.
         */
        private void rebuild_ui() {
            clear_widget(this.listbox);

            if (this.playlist != null) {
                foreach (Cellar.Segment segment in this.playlist.get_segments()) {
                    this.listbox.add(new PlaylistViewRow(segment, this.preview));
                }

                this.select_correct_row();
            }
        }

        /**
         * If this is a preview, deselects everything. Otherwise, selects the
         * segment that the current slide is in.
         */
        private void select_correct_row() {
            if (this.preview) {
                this.listbox.select_row(null);
            } else {
                int segment = Halogen.State.get_instance().segment_index;
                Gtk.ListBoxRow row = this.listbox.get_row_at_index(segment);
                this.listbox.select_row(row);
            }
        }


        private void on_segment_added(Cellar.Segment segment, int index) {
            var row = new PlaylistViewRow(segment, this.preview);
            this.listbox.insert(row, index);

            // if a header segment is added, you probably want to change its
            // name immediately
            if (segment.segment_type == HEADER) row.rename_header();
            this.select_correct_row();
        }

        private void on_segment_removed(int index) {
            Gtk.ListBoxRow row = this.listbox.get_row_at_index(index);
            this.listbox.remove(row);
            this.select_correct_row();
        }

        private void on_segment_moved(int from, int to) {
            this.rebuild_ui();
        }

        [GtkCallback]
        private void on_row_selected(Gtk.ListBoxRow? row) {
            this.select_correct_row();
        }

        [GtkCallback]
        private void on_row_activated(Gtk.ListBoxRow row) {
            if (!this.preview) {
                PresenterConsole.get_instance().slides.scroll_to_segment(row.get_index());
            }
        }

        [GtkCallback]
        private void on_drag_leave(Gdk.DragContext ctx, uint time) {
            listbox_remove_highlight(this.listbox);
        }

        [GtkCallback]
        private void on_drag_end(Gdk.DragContext ctx) {
            listbox_remove_highlight(this.listbox);
        }

        [GtkCallback]
        private bool on_drag_motion(Gdk.DragContext ctx, int x, int y, uint time) {
            listbox_set_highlight(this.listbox, y);
            return true;
        }

        [GtkCallback]
        private void on_drag_data_received(Gdk.DragContext ctx,
                                           int x, int y,
                                           Gtk.SelectionData selection,
                                           uint info,
                                           uint time) {
            string text = (string) selection.get_data();

            Gtk.ListBoxRow dest_row = this.listbox.get_row_at_y(y);
            int dest;
            if (dest_row == null) {
                dest = (int) this.listbox.get_children().length();
            } else {
                Gtk.Allocation alloc;
                dest_row.get_allocation(out alloc);
                if (y < alloc.y + alloc.height / 2) {
                    dest = dest_row.get_index();
                } else {
                    dest = dest_row.get_index() + 1;
                }
            }

            if (info == get_drag_target_segment().info) {
                int src = int.parse(text);
                Halogen.State.get_instance().playlist.move_segment(src, dest);
            } else if (info == get_drag_target_resources().info) {
                string[] ids = text.split(";");

                foreach (string id in ids) {
                    Cellar.Resource resource = Cellar.Library.get_instance()
                                               .get_resource(id);
                    if (resource != null) {
                        resource.update_access_time();
                        Cellar.Segment segment = new Cellar.Segment.from_resource(resource);
                        Halogen.State.get_instance().playlist.insert_segment(segment, dest);
                        dest ++;
                    }
                }
            }
        }
    }


    [GtkTemplate (ui="/io/gitlab/floodlight/Presenter/ui/common/playlist-view-row.ui")]
    public class PlaylistViewRow : Gtk.ListBoxRow {
        [GtkChild] private unowned Gtk.Label slideshow_name;
        [GtkChild] private unowned Gtk.Label header_name;
        [GtkChild] private unowned Gtk.Label invalid_name;
        [GtkChild] private unowned Gtk.Image thumbnail;
        [GtkChild] private unowned Gtk.Stack stack;
        [GtkChild] private unowned Gtk.Button play_button;
        [GtkChild] private unowned Gtk.Menu menu;
        [GtkChild] private unowned Gtk.Entry header_rename;
        [GtkChild] private unowned Gtk.Stack header_stack;
        [GtkChild] private unowned Gtk.MenuItem add_header;
        [GtkChild] private unowned Gtk.EventBox evbox;

        private Cellar.Segment segment;
        private bool preview;

        public PlaylistViewRow(Cellar.Segment segment, bool preview) {
            this.segment = segment;
            this.preview = preview;

            this.segment.changed.connect(() => this.rebuild_ui());
            this.rebuild_ui();
        }


        /**
         * If this is a header segment, opens the UI to rename the header.
         */
        public void rename_header() {
            if (this.segment.segment_type == HEADER) {
                this.header_rename.text = this.segment.name;
                this.header_rename.grab_focus();
                this.header_stack.visible_child_name = "edit";
            }
        }


        private void rebuild_ui() {
            switch (segment.segment_type) {
                case INVALID:
                    this.stack.visible_child_name = "invalid";
                    this.invalid_name.label = segment.name;
                    this.add_header.show();
                    break;
                case HEADER:
                    this.stack.visible_child_name = "header";
                    this.header_name.label = segment.name;
                    this.add_header.hide();
                    break;
                case SLIDESHOW:
                    this.stack.visible_child_name = "slideshow";
                    this.slideshow_name.label = segment.name;
                    this.add_header.show();

                    Thumbnails.thumbnail_slide(this.thumbnail, segment, null, 0, 24);

                    if (this.preview) this.play_button.hide();
                    break;
            }
        }

        /**
         * Gets the index of this row's segment. Does not work in preview mode.
         */
        private int get_segment_index() {
            if (this.preview) return -1;
            return Halogen.State.get_instance().playlist.get_segments()
                   .index_of(this.segment);
        }


        [GtkCallback]
        private void on_realize() {
            if (!this.preview) {
                Gtk.drag_source_set(
                    this.evbox,
                    Gdk.ModifierType.BUTTON1_MASK,
                    { get_drag_target_segment() },
                    Gdk.DragAction.COPY
                );
            }
        }

        [GtkCallback]
        private void on_play_button_clicked() {
            int index = this.get_segment_index();
            Halogen.State.get_instance().set_position(index, -1);
        }

        [GtkCallback]
        private bool on_button_press_event(Gtk.Widget widget, Gdk.EventButton event) {
            if (this.preview) return false;

            if (event.button == Gdk.BUTTON_SECONDARY) {
                this.menu.popup_at_pointer(event);
            }
            return false;
        }

        [GtkCallback]
        private void on_edit() {
            if (this.segment.segment_type == HEADER) {
                this.rename_header();
            }
        }

        [GtkCallback]
        private void on_rename_header() {
            this.segment.name = this.header_rename.text;
            this.header_stack.visible_child_name = "label";
        }

        [GtkCallback]
        private void on_add_header() {
            if (this.preview) return;
            int index = this.get_segment_index();
            Halogen.State.get_instance().playlist.insert_header(_("New Header"), index);
        }

        [GtkCallback]
        private void on_remove() {
            if (this.preview) return;
            int index = this.get_segment_index();
            Halogen.State.get_instance().playlist.remove_segment(index);
        }

        [GtkCallback]
        private void on_drag_begin(Gdk.DragContext ctx) {
            if (this.thumbnail.storage_type == SURFACE) {
                Gtk.drag_set_icon_surface(ctx, this.thumbnail.surface);
            }
        }

        [GtkCallback]
        private void on_drag_data_get(Gdk.DragContext ctx,
                                      Gtk.SelectionData selection,
                                      uint info, uint time) {
            string data = this.get_segment_index().to_string();
            Gtk.TargetEntry target = get_drag_target_segment();
            selection.@set(
                Gdk.Atom.intern(target.target, false),
                0, // no idea what this does but whatevz
                data.data
            );
        }
    }
}
