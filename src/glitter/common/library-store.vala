namespace Glitter {
    public class LibraryStore : Gtk.TreeStore {
        private static LibraryStore instance;
        public static LibraryStore get_instance() {
            if (instance == null) instance = new LibraryStore();
            return instance;
        }


        private LibraryStore() {
            this.set_column_types({
                Type.STRING,    // uuid
                Type.STRING,    // name
                Type.STRING,    // type
                Type.STRING,    // subinfo
                Type.UINT64     // last access
            });

            var library = Cellar.Library.get_instance();
            library.resource_added.connect(this.on_resource_added);
            library.resource_changed.connect(this.on_resource_changed);
            library.resource_removed.connect(this.on_resource_removed);

            foreach (var resource in library.get_all_resources()) {
                this.on_resource_added(resource);
            }
        }


        private Gtk.TreeIter? get_iter(string uuid) {
            Gtk.TreeIter? res = null;
            this.@foreach((model, path, iter) => {
                Value v_uuid;
                model.get_value(iter, 0, out v_uuid);
                if (v_uuid.get_string() == uuid) {
                    res = iter;
                    return true;
                }
                return false;
            });
            return res;
        }

        private void on_resource_added(Cellar.Resource resource) {
            Gtk.TreeIter? parent = resource.parent != null
                                   ? this.get_iter(resource.parent.get_full_id())
                                   : null;

            this.insert_with_values(null, parent, -1,
                0, resource.get_full_id(),
                1, resource.name,
                2, resource.resource_type.id,
                3, resource.subinfo,
                4, resource.last_access
            );

            foreach (var subres in resource.get_subresources()) {
                this.on_resource_added(subres);
            }
        }

        private void on_resource_changed(Cellar.Resource resource) {
            var iter = this.get_iter(resource.get_full_id());
            this.@set(iter,
                0, resource.get_full_id(),
                1, resource.name,
                2, resource.resource_type.id,
                3, resource.subinfo,
                4, resource.last_access
            );
        }

        private void on_resource_removed(Cellar.Resource resource) {
            var iter = this.get_iter(resource.get_full_id());
            this.remove(ref iter);
        }
    }
}
