namespace Glitter {
    [GtkTemplate (ui="/io/gitlab/floodlight/Presenter/ui/common/slide-editor.ui")]
    public class SlideEditor : Gtk.Box {
        /**
         * Whether the slide editor is part of the "quick edit" popover rather
         * than the slideshow editor.
         */
        public bool is_quick_edit { construct; get; default=false; }


        private Cellar.Slide _slide;
        public Cellar.Slide slide {
            get {
                return _slide;
            }
            set {
                if (_slide != null) _slide.changed.disconnect(this.rebuild_ui);

                _slide = value;
                if (_slide != null) _slide.changed.connect(this.rebuild_ui);

                // make sure the field widgets get rebuilt
                this.current_layout = null;

                this.rebuild_ui();
            }
        }

        /**
         * The segment the slide is part of.
         */
        private Cellar.Segment segment;

        /**
         * The index of the slide within the slideshow.
         */
        private int index;


        [GtkChild] private unowned Gtk.ComboBox layout_chooser;
        [GtkChild] private unowned Gtk.CheckButton continue_checkbox;
        [GtkChild] private unowned Gtk.Revealer continue_revealer;
        [GtkChild] private unowned Gtk.ListStore layout_store;
        [GtkChild] private unowned Gtk.Grid fields;
        [GtkChild] private unowned Gtk.Button delete_button;
        [GtkChild] private unowned Gtk.RadioButton continue_mode_timer;
        [GtkChild] private unowned Gtk.RadioButton continue_mode_end;
        [GtkChild] private unowned Gtk.SpinButton continue_delay;


        /**
         * The layout that the field UI is currently set up for.
         */
        private string? current_layout;

        private bool rebuild_in_progress = false;


        construct {
            foreach (var layout in Halogen.LayoutManager.get_instance().layouts) {
                this.layout_store.insert_with_values(null, -1,
                    0, layout.id,
                    1, layout.name
                );
            }

            this.rebuild_ui();

            this.continue_checkbox.bind_property(
                "active", this.continue_revealer, "reveal-child", SYNC_CREATE
            );
            this.continue_mode_timer.bind_property(
                "active", this.continue_delay, "sensitive", SYNC_CREATE
            );
        }


        /**
         * Sets the segment information, to enable features like "Delete Slide".
         * This is used in Quick Edit popovers for internal slideshows, since
         * they can't be edited elsewhere.
         */
        public void set_segment_info(Cellar.Segment segment, int index) {
            this.segment = segment;
            this.index = index;
            this.delete_button.visible = true;
        }


        private void rebuild_ui() {
            if (this.slide == null) {
                this.hide();
                return;
            }

            this.rebuild_in_progress = true;

            this.layout_chooser.active_id = this.slide.layout_id;

            this.continue_checkbox.active = this.slide.continue_mode != MANUAL;
            this.continue_mode_timer.active = this.slide.continue_mode == TIMER;
            this.continue_mode_end.active = this.slide.continue_mode == WHEN_FINISHED;
            this.continue_delay.value = this.slide.continue_delay;

            if (this.slide.layout_id != this.current_layout) {
                clear_widget(this.fields);

                var layout = Halogen.LayoutManager.get_instance().get_layout(this.slide.layout_id);

                int row = 0;
                int row_count = layout.get_field_list().size;
                foreach (Halogen.LayoutField field in layout.get_field_list()) {
                    if (row_count != 1) {
                        var label = new Gtk.Label(field.name);
                        label.get_style_context().add_class("dim-label");
                        label.halign = END;
                        label.show();
                        this.fields.attach(label, 0, row);
                    }

                    if (field.muted) {
                        Gtk.Image muted = new Gtk.Image.from_icon_name(
                            "audio-volume-muted-symbolic", BUTTON
                        );
                        muted.tooltip_text = _("This media will not play audio");
                        muted.get_style_context().add_class("dim-label");
                        muted.show();
                        this.fields.attach(muted, 1, row);
                    }

                    Gtk.Widget widget = this.create_field_widget(this.slide, field);
                    widget.show();
                    this.fields.attach(widget, 2, row);

                    row ++;
                }

                this.current_layout = this.slide.layout_id;
            }

            this.show();
            this.rebuild_in_progress = false;
        }

        private Gtk.Widget create_field_widget(Cellar.Slide slide, Halogen.LayoutField field) {
            switch (field.field_type) {
                case RESOURCE:
                    return new ResourceField(slide, field.id);
                case TEXT:
                    return new TextField(slide, field.id);
                case MULTILINE:
                    return new MultilineField(slide, field.id);
                default:
                    assert_not_reached();
            }
        }


        [GtkCallback]
        private void on_continue_mode_toggled() {
            if (this.rebuild_in_progress) return;

            if (this.continue_checkbox.active) {
                this.slide.continue_mode = WHEN_FINISHED;
            } else {
                this.slide.continue_mode = MANUAL;
            }
        }

        [GtkCallback]
        private void on_continue_end_toggled() {
            if (this.rebuild_in_progress) return;

            if (this.continue_mode_end.active) {
                this.slide.continue_mode = WHEN_FINISHED;
            } else {
                this.slide.continue_mode = TIMER;
            }
        }

        [GtkCallback]
        private void on_continue_delay_changed() {
            if (this.rebuild_in_progress) return;

            this.slide.continue_delay = this.continue_delay.value;
        }

        [GtkCallback]
        private void on_layout_changed() {
            this.slide.layout_id = this.layout_chooser.active_id;
        }

        [GtkCallback]
        private void on_delete_clicked() {
            var arrangement = this.segment.slideshow.arrangement;
            int sindex = arrangement.slide_to_content_index(this.index);
            arrangement.remove_content(sindex);
        }
    }


    private class TextField : Gtk.Entry {
        private Cellar.Slide slide;
        private string field_id;


        public TextField(Cellar.Slide slide, string field_id) {
            this.slide = slide;
            this.field_id = field_id;

            this.text = slide.get_field(this.field_id) ?? "";

            slide.field_changed.connect(this.on_field_changed);
            this.changed.connect(this.on_value_changed);
        }


        private void on_field_changed(string key, string val) {
            if (key == this.field_id && this.text != val) {
                this.changed.disconnect(this.on_value_changed);
                this.text = val;
                this.changed.connect(this.on_value_changed);
            }
        }

        private void on_value_changed() {
            slide.set_field(this.field_id, this.text);
        }
    }


    private class MultilineField : Gtk.TextView {
        private Cellar.Slide slide;
        private string field_id;


        public MultilineField(Cellar.Slide slide, string field_id) {
            this.slide = slide;
            this.field_id = field_id;

            this.buffer.text = slide.get_field(this.field_id) ?? "";

            slide.field_changed.connect(this.on_field_changed);
            this.buffer.changed.connect(this.on_value_changed);

            this.width_request = 250;
            this.height_request = 100;
            this.left_margin = this.right_margin =
                this.top_margin = this.bottom_margin = 6;

            this.hexpand = true;
        }


        private void on_field_changed(string key, string val) {
            if (key == this.field_id && this.buffer.text != val) {
                this.buffer.changed.disconnect(this.on_value_changed);
                this.buffer.text = val;
                this.buffer.changed.connect(this.on_value_changed);
            }
        }

        private void on_value_changed() {
            slide.set_field(this.field_id, this.buffer.text);
        }
    }


    private class ResourceField : ResourceChooser {
        private Cellar.Slide slide;
        private string field_id;


        public ResourceField(Cellar.Slide slide, string field_id) {
            this.slide = slide;
            this.field_id = field_id;

            string resource = slide.get_field(this.field_id);
            this.on_field_changed(this.field_id, resource);

            slide.field_changed.connect(this.on_field_changed);
            this.resource_chosen.connect(this.on_value_changed);

            this.hexpand = true;
        }


        private void on_field_changed(string key, string? val) {
            if (key == this.field_id) {
                if (val == null) {
                    this.resource = null;
                } else {
                    this.resource = Cellar.Library.get_instance().get_resource(val);
                }
            }
        }

        private void on_value_changed(Cellar.Resource? resource) {
            if (this.resource == null) {
                slide.set_field(this.field_id, null);
            } else {
                slide.set_field(this.field_id, this.resource.uuid);
                this.resource.update_access_time();
            }
        }
    }
}
