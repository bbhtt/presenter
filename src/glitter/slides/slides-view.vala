namespace Glitter {
    [GtkTemplate (ui="/io/gitlab/floodlight/Presenter/ui/slides/slides-view.ui")]
    public class SlidesView : Gtk.ScrolledWindow {
        [GtkChild] private unowned Gtk.Box segments;


        private Cellar.Playlist playlist;


        construct {
            Halogen.State.get_instance().notify["playlist"].connect(() => {
                this.rebuild_ui();
            });
            this.rebuild_ui();

            Halogen.State.get_instance().user_changed_slide.connect(() => {
                this.scroll_to_current();
            });

            Cellar.Library.get_instance().resource_changed.connect(this.selectively_rerender);
            Cellar.Library.get_instance().resource_removed.connect(this.selectively_rerender);
        }


        /**
         * Scrolls the view to the given segment index. Used by the playlist
         * view when you click a row.
         */
        public void scroll_to_segment(int index) {
            Gtk.Widget child = this.segments.get_children().nth_data(index);
            Gtk.Allocation alloc;
            child.get_allocation(out alloc);
            this.vadjustment.set_value(alloc.y);
        }

        /**
         * Scrolls the view to the current slide.
         */
        public void scroll_to_current() {
            var state = Halogen.State.get_instance();
            if (state.floating) return;
            int segment_index = state.segment_index;
            int slide_index = state.slide_index;

            Gtk.Widget widget = this.segments.get_children().nth_data(segment_index);

            if (!(widget is SlideshowSegment)) {
                warning("Slides view is out of sync, expected SlideshowSegment!");
                return;
            }
            SlideshowSegment child = (SlideshowSegment) widget;

            Gtk.Allocation alloc;
            child.get_allocation(out alloc);
            this.vadjustment.set_value(alloc.y + child.get_slide_y_pos(slide_index));
        }


        private Gtk.Widget create_segment(Cellar.Segment segment) {
            switch (segment.segment_type) {
                case HEADER:
                    return new HeaderSegment(segment);
                case SLIDESHOW:
                    return new SlideshowSegment(segment);
                case INVALID:
                    return new InvalidSegment(segment);
                default:
                    assert_not_reached();
            }
        }

        private void rebuild_ui() {
            if (this.playlist != null) {
                this.playlist.segment_added.disconnect(this.on_segment_added);
                this.playlist.segment_removed.disconnect(this.on_segment_removed);
                this.playlist.segment_moved.disconnect(this.on_segment_moved);
            }

            this.playlist = Halogen.State.get_instance().playlist;

            this.playlist.segment_added.connect(this.on_segment_added);
            this.playlist.segment_removed.connect(this.on_segment_removed);
            this.playlist.segment_moved.connect(this.on_segment_moved);


            clear_widget(this.segments);

            foreach (Cellar.Segment segment in this.playlist.get_segments()) {
                this.segments.add(this.create_segment(segment));
            }
        }

        private void selectively_rerender(Cellar.Resource resource) {
            foreach (var widget in this.segments.get_children()) {
                if (widget is SlideshowSegment) {
                    ((SlideshowSegment) widget).selectively_rerender(resource);
                }
            }
        }

        private void on_segment_added(Cellar.Segment segment, int index) {
            var row = this.create_segment(segment);
            this.segments.add(row);
            this.segments.reorder_child(row, index);
        }

        private void on_segment_removed(int index) {
            Gtk.Widget widget = this.segments.get_children().nth_data(index);
            this.segments.remove(widget);
        }

        private void on_segment_moved(int from, int to) {
            Gtk.Widget widget = this.segments.get_children().nth_data(from);
            this.segments.reorder_child(widget, to);
        }
    }
}
