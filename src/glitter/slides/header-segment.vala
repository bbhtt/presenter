namespace Glitter {
    [GtkTemplate (ui="/io/gitlab/floodlight/Presenter/ui/slides/header-segment.ui")]
    public class HeaderSegment : Gtk.Box {
        [GtkChild] private unowned Gtk.Stack name_stack;
        [GtkChild] private unowned Gtk.Label name_label;
        [GtkChild] private unowned Gtk.Entry rename;


        private Cellar.Segment segment;


        public HeaderSegment(Cellar.Segment segment) {
            this.segment = segment;

            this.segment.bind_property("name", this.name_label, "label", SYNC_CREATE);
        }


        [GtkCallback]
        private void on_rename() {
            if (this.name_stack.visible_child_name == "edit") {
                this.segment.name = this.rename.text;
                this.name_stack.visible_child_name = "view";
            } else {
                this.rename.text = this.segment.name;
                this.name_stack.visible_child_name = "edit";
                this.rename.grab_focus();
            }
        }
    }
}
