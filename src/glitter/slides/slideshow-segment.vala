namespace Glitter {
    [GtkTemplate (ui="/io/gitlab/floodlight/Presenter/ui/slides/slideshow-segment.ui")]
    public class SlideshowSegment : Gtk.Box {
        [GtkChild] private unowned Gtk.Label name_label;
        [GtkChild] private unowned Gtk.Stack expand_icon_stack;
        [GtkChild] private unowned Gtk.Revealer slides_revealer;
        [GtkChild] private unowned Gtk.FlowBox slides;
        [GtkChild] private unowned Gtk.Stack empty_state_stack;
        [GtkChild] private unowned Gtk.ToggleButton loop_toggle;
        [GtkChild] private unowned Gtk.Button edit_button;
        [GtkChild] private unowned Gtk.ToggleButton comments_button;
        [GtkChild] private unowned Gtk.ToggleButton settings_button;
        [GtkChild] private unowned Gtk.Stack settings_stack;
        [GtkChild] private unowned Gtk.TextBuffer comments_buffer;
        [GtkChild] private unowned ResourceChooser background_chooser;
        [GtkChild] private unowned Gtk.Image loop_icon;
        [GtkChild] private unowned Gtk.Button add_to_library_button;


        private Cellar.Segment segment;


        public SlideshowSegment(Cellar.Segment segment) {
            this.segment = segment;

            this.segment.bind_property("name", this.name_label, "label", SYNC_CREATE);
            this.segment.bind_property("looping", this.loop_toggle, "active", SYNC_CREATE | BIDIRECTIONAL);
            this.segment.bind_property("looping", this.loop_icon, "visible", SYNC_CREATE);

            var state = Halogen.State.get_instance();
            state.notify["segment"].connect(this.update_selection);
            state.notify["slide"].connect(this.update_selection);
            // Also update on indexes, in case the slide/segment is moved
            state.notify["segment-index"].connect(this.update_selection);
            state.notify["slide-index"].connect(this.update_selection);
            segment.changed.connect(this.rebuild_ui);

            this.segment.bind_property("comments", this.comments_buffer, "text", BIDIRECTIONAL);
            if ((this.segment.comments ?? "") != "") {
                this.comments_buffer.text = this.segment.comments;
                this.comments_button.active = true;
            }

            Gtk.drag_dest_set(
                this.slides,
                Gtk.DestDefaults.ALL,
                { get_drag_target_resources() },
                Gdk.DragAction.COPY
            );

            this.rebuild_ui();
        }


        /**
         * Gets the Y position of the slide icon at the given index. Used for
         * scrolling the slide into view.
         */
        public int get_slide_y_pos(int slide_index) {
            Gtk.Allocation alloc;
            this.slides.get_child_at_index(slide_index).get_allocation(out alloc);
            return alloc.y;
        }

        /**
         * Rerenders any of the thumbnails that use the given resource, because
         * it was changed or removed.
         */
        public void selectively_rerender(Cellar.Resource resource) {
            foreach (var widget in this.slides.get_children()) {
                ((Slide) widget).selectively_rerender(resource);
            }
        }


        private void rebuild_ui() {
            this.add_to_library_button.visible = (this.segment.slideshow.resource == null);
            if (this.segment.slideshow.resource == null) {
                this.edit_button.hide();
            } else {
                this.edit_button.show();
            }

            clear_widget(this.slides);
            var slides = this.segment.slideshow.arrangement.get_slides();
            this.empty_state_stack.visible_child_name = slides.size > 0 ? "nonempty" : "empty";
            int index = 0;
            foreach (Cellar.Slide slide in slides) {
                this.slides.add(new Slide(segment, slide, index));
                index ++;
            }

            this.background_chooser.resource = this.segment.background;

            this.update_selection();
        }

        private void update_selection() {
            var state = Halogen.State.get_instance();
            if (state.segment == segment) {
                Gtk.FlowBoxChild child = this.slides.get_child_at_index(state.slide_index);
                if (child != null) {
                    this.slides.select_child(child);
                    this.toggle_expand(true);
                }
            } else {
                this.slides.unselect_all();
            }
        }

        [GtkCallback]
        private void on_background_chosen(Cellar.Resource? resource) {
            this.segment.background = resource;
            if (resource != null) resource.update_access_time();
        }

        [GtkCallback]
        private void on_selection_changed() {
            this.update_selection();
        }

        [GtkCallback]
        private void on_slide_activated(Gtk.FlowBoxChild child) {
            var state = Halogen.State.get_instance();

            Gee.List<Cellar.Segment> segments = state.playlist.get_segments();
            int segment = segments.index_of(this.segment);
            int slide = child.get_index();

            state.set_position(segment, slide);
        }

        private void toggle_expand(bool toggle) {
            if (toggle) {
                this.slides_revealer.reveal_child = true;
                this.expand_icon_stack.visible_child_name = "expanded";
            } else {
                this.slides_revealer.reveal_child = false;
                this.expand_icon_stack.visible_child_name = "collapsed";
            }
        }

        [GtkCallback]
        private bool on_toggle_expand(Gdk.EventButton event) {
            this.toggle_expand(!this.slides_revealer.reveal_child);
            return false;
        }

        [GtkCallback]
        private void on_edit_clicked() {
            var info = new Glitter.TabInfo(
                EDIT_SLIDESHOW, this.segment.slideshow.resource.get_full_id(),
                null
            );
            PresenterConsole.get_instance().tabs.show_tab(info);
        }

        [GtkCallback]
        private void on_comments_button_toggled() {
            if (this.comments_button.active) {
                this.settings_stack.visible_child_name = "comments";
                this.settings_button.active = false;
            } else if (this.settings_stack.visible_child_name == "comments") {
                this.settings_stack.visible_child_name = "none";
            }
        }

        [GtkCallback]
        private void on_settings_button_toggled() {
            if (this.settings_button.active) {
                this.settings_stack.visible_child_name = "settings";
                this.comments_button.active = false;
            } else if (this.settings_stack.visible_child_name == "settings") {
                this.settings_stack.visible_child_name = "none";
            }
        }

        [GtkCallback]
        private void on_add_to_library_clicked() {
            this.segment.add_to_library();
        }

        [GtkCallback]
        private void on_drag_leave(Gdk.DragContext ctx, uint time) {
            flowbox_remove_highlight(this.slides);
        }

        [GtkCallback]
        private void on_drag_end(Gdk.DragContext ctx) {
            flowbox_remove_highlight(this.slides);
        }

        [GtkCallback]
        private bool on_drag_motion(Gdk.DragContext ctx, int x, int y, uint time) {
            flowbox_set_highlight(this.slides, x, y);
            return false;
        }

        [GtkCallback]
        private void on_drag_data_received(Gdk.DragContext ctx,
                                           int x, int y,
                                           Gtk.SelectionData selection,
                                           uint info,
                                           uint time) {
            if (info != get_drag_target_resources().info) return;
            string text = (string) selection.get_data();

            Gtk.FlowBoxChild dest_child = this.slides.get_child_at_pos(x, y);
            int dest;
            if (dest_child == null) {
                dest = (int) this.slides.get_children().length();
            } else {
                Gtk.Allocation alloc;
                dest_child.get_allocation(out alloc);
                if (x < alloc.x + alloc.width / 2) {
                    dest = dest_child.get_index();
                } else {
                    dest = dest_child.get_index() + 1;
                }
            }

            string[] ids = text.split(";");
            foreach (string id in ids) {
                Cellar.Resource resource = Cellar.Library.get_instance()
                                           .get_resource(id);
                if (resource == null) continue;
                if (!resource.resource_type.is_media()) continue;

                resource.update_access_time();
                var slide = new Cellar.Slide.for_media(resource);

                segment.slideshow.arrangement.insert_slide(slide, dest);
                dest ++;
            }

            Gtk.drag_finish(ctx, true, false, time);
        }
    }
}
