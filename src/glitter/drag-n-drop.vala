namespace Glitter {
    public Gtk.TargetEntry get_drag_target_none() {
        return Gtk.TargetEntry() {
            target="none",
            info=1,
            flags=Gtk.TargetFlags.SAME_APP
        };
    }

    public Gtk.TargetEntry get_drag_target_resources() {
        return Gtk.TargetEntry() {
            target="resources",
            info=2,
            flags=Gtk.TargetFlags.SAME_APP
        };
    }

    public Gtk.TargetEntry get_drag_target_segment() {
        return Gtk.TargetEntry() {
            target="segment",
            info=3,
            flags=Gtk.TargetFlags.SAME_APP
        };
    }

    public Gtk.TargetEntry get_drag_target_playlists() {
        return Gtk.TargetEntry() {
            target="playlist",
            info=4,
            flags=Gtk.TargetFlags.SAME_APP
        };
    }
}
