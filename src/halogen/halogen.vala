namespace Halogen {
    internal static bool initted = false;
    public void init(string[] args) {
        if (initted) return;

        Gst.init(ref args);

        initted = true;
    }
}
