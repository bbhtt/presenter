namespace Halogen {
    /**
     * Abstract base class for feeds.
     */
    public abstract class Feed : Object {
        public string id;

        protected Json.Object json;

        protected int width;
        protected int height;


        protected Feed(string id, Json.Object json) {
            this.id = id;
            this.json = json;
        }


        public virtual void resize(int width, int height) {
            this.width = width;
            this.height = height;
        }

        /**
         * Renders the feed to the context at the given coordinates. The
         * feed's dimensions should have already been set using resize().
         */
        public virtual void render(Cairo.Context cr, int x, int y) {}
    }

    public class RegularFeed : Feed {
        /**
         * The current layout renderer.
         */
        private LayoutRenderer? renderer;

        /**
         * The LayoutRenderer that is currently fading out, if there is one.
         */
        private LayoutRenderer? fading_out;

        /**
         * Crossfade timer.
         */
        private Timer? xfade;

        /**
         * The surface to draw the fading out layout on.
         */
        private Cairo.ImageSurface xfade_surf;


        public RegularFeed(string id, Json.Object json) {
            base(id, json);
            this.xfade_surf = new Cairo.ImageSurface(Cairo.Format.ARGB32, 1, 1);

            State.get_instance().slide_changed.connect(this.update_slide);
            this.update_slide();
        }


        public override void render(Cairo.Context cr, int x, int y) {
        lock (this.renderer) {
            cr.save();
            if (this.renderer != null) {
                this.renderer.render(cr, x, y, this.width, this.height);
            } else {
                cr.rectangle(x, y, this.width, this.height);
                cr.set_source_rgb(0, 0, 0);
                cr.fill();
            }

            if (this.xfade != null) {
                double duration = Photon.Config.get_instance().crossfade_duration;
                double alpha = 1 - (this.xfade.elapsed() / duration);
                if (alpha < 0) {
                    this.fading_out = null;
                    this.xfade = null;
                } else {
                    if (this.fading_out != null) {
                        Cairo.Context cr2 = new Cairo.Context(this.xfade_surf);
                        this.fading_out.render(cr2, 0, 0, this.width, this.height);

                        draw_to_context(
                            cr, this.xfade_surf,
                            x, y, this.width, this.height,
                            new DrawOpts(), alpha
                        );
                    } else {
                        cr.rectangle(x, y, this.width, this.height);
                        cr.set_source_rgba(0, 0, 0, alpha);
                        cr.fill();
                    }
                }
            }

            cr.restore();
        }
        }

        public override void resize(int width, int height) {
            base.resize(width, height);

            this.xfade_surf = new Cairo.ImageSurface(Cairo.Format.ARGB32, this.width, this.height);
        }


        private void update_slide() {
            Cellar.Segment segment = State.get_instance().segment;
            Cellar.Slide slide = State.get_instance().slide;

            lock (this.renderer) {
                this.xfade = new Timer();
                this.fading_out = this.renderer;

                if (slide == null) {
                    this.renderer = null;
                } else {
                    Layout layout = LayoutManager.get_instance().get_layout(slide.layout_id);
                    this.renderer = layout.page_for_feed(this.id).get_renderer(segment, slide);
                }
            }
        }
    }
}
