namespace Halogen {
    /**
     * A helper function for rendering text using Pango/Cairo.
     *
     * Returns: the height of the text
     */
    public double render_text(string text,
                              Cairo.Context cr,
                              TextOptions opts,
                              double x, double y, double w, double h,
                              int screen_h) {

        Pango.Layout layout = Pango.cairo_create_layout(cr);
        layout.set_text(text, -1);

        var desc = new Pango.FontDescription();
        desc.set_family(opts.font_family);
        desc.set_absolute_size(opts.height * screen_h * Pango.SCALE);
        layout.set_font_description(desc);

        layout.set_indent((int) (opts.indent * screen_h * Pango.SCALE));

        // text width, text height
        double tw, th;
        layout.get_pixel_size(out tw, out th);

        layout.set_width((int) (w * Pango.SCALE));

        switch (opts.wrap) {
            case NONE:
                layout.set_width(-1);
                break;
            case WORD:
                layout.set_wrap(Pango.WrapMode.WORD);
                break;
            case CHAR:
                layout.set_wrap(Pango.WrapMode.CHAR);
                break;
            case WORD_CHAR:
                layout.set_wrap(Pango.WrapMode.WORD_CHAR);
                break;
            case SHRINK:
                layout.set_width(-1);

                double old_tw = tw;
                shrink_to_fit(ref tw, ref th, w, h);
                if (old_tw == 0 || tw == 0) return 0;
                double ratio = tw / old_tw;

                desc.set_absolute_size(desc.get_size() * ratio);
                layout.set_font_description(desc);
                break;
        }
        layout.get_pixel_size(out tw, out th);

        switch (opts.valign) {
            case TOP:
                break;
            case BOTTOM:
                y += h - th;
                break;
            case MIDDLE:
                y += (h - th) / 2;
                break;
        }

        // if a width is set, then the layout will handle the math for us
        // hence `if (layout.get_width() == -1)`
        switch (opts.halign) {
            case LEFT:
                layout.set_alignment(Pango.Alignment.LEFT);
                break;
            case RIGHT:
                layout.set_alignment(Pango.Alignment.RIGHT);
                if (layout.get_width() == -1) x += w - tw;
                break;
            case CENTER:
                layout.set_alignment(Pango.Alignment.CENTER);
                if (layout.get_width() == -1) x += (w - tw) / 2;
                break;
        }

        cr.set_source_rgb(opts.color.r, opts.color.g, opts.color.b);
        cr.move_to(x, y);
        Pango.cairo_update_layout(cr, layout);
        Pango.cairo_show_layout(cr, layout);

        double out_h;
        layout.get_pixel_size(null, out out_h);
        return out_h;
    }


    /**
     * Options for rendering text.
     */
    public class TextOptions : Object {
        public enum HAlign {
            LEFT,
            RIGHT,
            CENTER,
        }

        public enum VAlign {
            TOP,
            BOTTOM,
            MIDDLE,
        }

        public enum Wrap {
            NONE,
            WORD,
            CHAR,
            WORD_CHAR,
            SHRINK,
        }

        public struct Color {
            double r;
            double g;
            double b;
        }


        public string font_family = "Cantarell";

        /**
         * Expressed as a fraction of the screen height.
         */
        public double height = 0.08;

        /**
         * As fraction of screen width. Negative for hanging indent.
         */
        public double indent = 0;

        public Color color = Color() { r = 1, g = 1, b = 1 };

        public HAlign halign = CENTER;
        public VAlign valign = MIDDLE;

        public Wrap wrap = WORD_CHAR;


        /**
         * Creates a new TextOptions with all the defaults.
         */
        public TextOptions.defaults() {
        }

        /**
         * Creates a new TextOptions with the defaults for the stage view lyrics.
         */
        public TextOptions.defaults_stageview() {
            this.halign = LEFT;
            this.valign = TOP;
            this.indent = -0.05;
            this.height = 0.075;
            this.wrap = WORD_CHAR;
        }

        /**
         * Creates a new TextOptions with the defaults for the stage view lyrics
         * for the next slide.
         */
        public TextOptions.defaults_stageview_next() {
            this.defaults_stageview();
            this.color = Color() { r = 0.75, g = 0.75, b = 0.75 };
        }

        /**
         * Creates a new TextOptions with the defaults for the stage view clock.
         */
        public TextOptions.defaults_clock() {
            this.font_family = "monospace";
            this.color = Color() { r = 1, g = 0, b = 0 };
            this.halign = RIGHT;
            this.valign = TOP;
            this.wrap = SHRINK;
        }

        /**
         * Creates a new TextOptions with the defaults for the stage view comments.
         */
        public TextOptions.defaults_comments() {
            this.halign = LEFT;
            this.valign = TOP;
            this.height = 0.05;
            this.wrap = WORD_CHAR;
        }

        /**
         * Loads options from JSON.
         *
         * Options not specified in the JSON object will be left to the
         * defaults. Changes to the TextOptions will not be reflected in the
         * JSON object.
         */
        public TextOptions.from_json(Json.Object obj) {
            if (obj.has_member("font-family")) {
                this.font_family = obj.get_string_member("font-family");
            }
            if (obj.has_member("height")) {
                this.height = obj.get_double_member("height");
            }
            if (obj.has_member("indent")) {
                this.indent = obj.get_double_member("indent");
            }
            if (obj.has_member("color")) {
                Json.Array colors = obj.get_array_member("color");
                if (colors.get_length() != 3) {
                    warning("Malformed TextOptions JSON! `color` must be a 3-part array");
                    return;
                }
                this.color = Color() {
                    r = colors.get_double_element(0),
                    g = colors.get_double_element(1),
                    b = colors.get_double_element(2)
                };
            }


            if (obj.has_member("halign")) {
                switch (obj.get_string_member("halign")) {
                    case "left": this.halign = HAlign.LEFT; break;
                    case "right": this.halign = HAlign.RIGHT; break;
                    case "center": this.halign = HAlign.CENTER; break;
                }
            }

            if (obj.has_member("valign")) {
                switch (obj.get_string_member("valign")) {
                    case "top": this.valign = VAlign.TOP; break;
                    case "bottom": this.valign = VAlign.BOTTOM; break;
                    case "middle": this.valign = VAlign.MIDDLE; break;
                }
            }
            if (obj.has_member("wrap")) {
                switch (obj.get_string_member("wrap")) {
                    case "none": this.wrap = Wrap.NONE; break;
                    case "word": this.wrap = Wrap.WORD; break;
                    case "char": this.wrap = Wrap.CHAR; break;
                    case "word-char": this.wrap = Wrap.WORD_CHAR; break;
                    case "shrink": this.wrap = Wrap.SHRINK; break;
                }
            }
        }
    }
}
