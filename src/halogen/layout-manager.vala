using Gee;

namespace Halogen {
    /**
     * Manages layouts, which describe how different types of slides should be
     * rendered.
     */
    public class LayoutManager : Object {
        private static LayoutManager instance;
        public static LayoutManager get_instance() {
            if (instance == null) instance = new LayoutManager();
            return instance;
        }


        public Gee.Collection<Layout> layouts;


        private HashMap<string, Layout> layout_map;

        private File file;
        private File dir;
        private Json.Object json;


        private LayoutManager() {
            this.layout_map = new HashMap<string, Layout>();
            this.layouts = this.layout_map.values;

            var config = Photon.Config.get_instance();
            this.file = File.new_for_path(config.datadir)
                        .get_child("layouts")
                        .get_child("index.json");
            this.dir = File.new_for_path(config.datadir)
                       .get_child("layouts");

            try {
                var parser = new Json.Parser();
                parser.load_from_file(this.file.get_path());
                this.json = parser.get_root().get_object();
            } catch (Error e) {
                debug("No layout index found, starting anew");
                var parser = new Json.Parser();

                try {
                    File defaults = File.new_for_uri("resource:///io/gitlab/floodlight/Presenter/defaults/layout-index.json");
                    parser.load_from_stream(defaults.read());
                    this.json = parser.get_root().get_object();
                } catch (Error e) {
                    critical("ERROR! COULD NOT CREATE INITIAL LAYOUT INDEX: %s", e.message);
                }

                this.save();
            }

            this.json.foreach_member((obj, key, val) => {
                var layout = new Layout(key, val.get_object());
                this.layout_map[key] = layout;
            });
        }


        public Layout? get_layout(string id) {
            return this.layout_map[id];
        }


        private void save() {
            Photon.save_json(this.file, this.json);
        }
    }
}
