namespace Halogen {
    public class FeedManager : Object {
        private static FeedManager instance;
        public static FeedManager get_instance() {
            if (instance == null) instance = new FeedManager();
            return instance;
        }


        /**
         * A map of feeds by their IDs (not necessarily UUIDs).
         */
        private Gee.HashMap<string, Feed> feeds;

        private File file;

        private Json.Object json;


        private FeedManager() {
            this.feeds = new Gee.HashMap<string, Feed>();

            var config = Photon.Config.get_instance();
            this.file = File.new_for_path(config.datadir)
                        .get_child("config")
                        .get_child("feeds.json");

            try {
                var parser = new Json.Parser();
                parser.load_from_file(this.file.get_path());
                this.json = parser.get_root().get_object();
            } catch (Error e) {
                debug("No feed config found, starting anew");
                var parser = new Json.Parser();

                try {
                    File defaults = File.new_for_uri("resource:///io/gitlab/floodlight/Presenter/defaults/feed-config.json");
                    parser.load_from_stream(defaults.read());
                    this.json = parser.get_root().get_object();
                } catch (Error e) {
                    critical("ERROR! COULD NOT CREATE INITIAL FEED CONFIG: %s", e.message);
                }
            }
        }


        /**
         * Creates a new Feed with the given ID.
         */
        public Feed? create_feed(string id) {
            if (!this.json.has_member(id)) return null;
            Json.Object obj = this.json.get_object_member(id);

            if (obj.get_boolean_member("stageview")) {
                return new StageViewFeed(id, obj);
            } else {
                return new RegularFeed(id, obj);
            }
        }

        /**
         * Returns whether the feed is a stage view.
         */
        public bool is_stageview_feed(string id) {
            if (!this.json.has_member(id)) return false;
            Json.Object obj = this.json.get_object_member(id);
            return obj.get_boolean_member("stageview");
        }

        /**
         * Returns a map of feed names by their IDs.
         */
        public Gee.HashMap<string, string> get_feeds() {
            var res = new Gee.HashMap<string, string>();
            this.json.foreach_member((obj, key, val) => {
                res[key] = val.get_object().get_string_member("name");
            });
            return res;
        }
    }
}
