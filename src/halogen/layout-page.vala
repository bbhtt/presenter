using Gee;

namespace Halogen {
    /**
     * Specifies how a layout page should be drawn on a stage view.
     */
    public enum StageViewMode {
        /**
         * Ignore any widgets that may be in the layout page, and instead let
         * the feed draw the stage view.
         */
        NONE,

        /**
         * Draw the layout next to the stage view sidebar.
         */
        PARTIAL,

        /**
         * Don't draw the stage view at all, and draw the layout fullscreen as
         * on a regular feed.
         */
        FULL,
    }


    /**
     * Represents a page of a layout. Pages can be reassigned to different
     * feeds, or be used on multiple feeds.
     */
    public class LayoutPage : Object {
        public string name { get; private set; }

        public StageViewMode stage_view_mode = NONE;

        /**
         * The Layout this LayoutPage belongs to.
         */
        public weak Layout layout { get; private set; }


        internal Gee.List<LayoutWidget> widgets;

        private Json.Object json;


        public LayoutPage(Layout layout, Json.Object json) {
            this.layout = layout;
            this.json = json;

            this.widgets = new Gee.ArrayList<LayoutWidget>();

            this.name = this.json.get_string_member("name");

            if (this.json.has_member("stage-view-mode")) {
                this.stage_view_mode = (StageViewMode) this.json.get_int_member("stage-view-mode");
            }

            this.json.get_array_member("widgets").foreach_element(
                (array, index, val) => {
                    this.widgets.add(new LayoutWidget(this, val.get_object()));
                }
            );
        }


        /**
         * Gets a new #LayoutRenderer to render this page onto a video stream.
         */
        public LayoutRenderer get_renderer(Cellar.Segment? segment, Cellar.Slide slide) {
            return new LayoutRenderer(this, segment, slide, false);
        }

        /**
         * Gets a new #LayoutRenderer to render this page onto a video stream.
         */
        public LayoutRenderer get_thumbnail_renderer(Cellar.Segment? segment, Cellar.Slide slide) {
            return new LayoutRenderer(this, segment, slide, true);
        }
    }
}
