int main(string[] args) {
    if ("--self-test" in args) {
        return floodlight_self_test(args);
    }

    if (Photon.IS_PACKAGED) {
        debug("Packaged");
        // These environment variables need to be set up in order for
        // Floodlight to work in a "packaged" environment, where we bring our
        // own libraries instead of relying on those from an existing Linux
        // environment.

        File executable = File.new_for_path(args[0]);
        string lib, res;
        #if PHOTON_IS_MACOS
            lib = executable.get_parent().get_parent().get_child("Libraries").get_path();
            res = executable.get_parent().get_parent().get_child("Resources").get_path();
            Environment.set_variable("XDG_DATA_HOME", Photon.macos_get_data_dir(), true);
        #else
            lib = executable.get_parent().get_path();
            res = executable.get_parent().get_parent().get_child("resources").get_path();
        #endif

        Environment.set_variable("GTK_PATH", res, true);
        Environment.set_variable("GDK_PIXBUF_MODULE_FILE", res + "/gdk-loaders.cache", true);
        Environment.set_variable("GTK_IM_MODULE_FILE", res + "/immodules.cache", true);

        Environment.set_variable("GST_PLUGIN_PATH", lib, true);
        Environment.set_variable("GST_PLUGIN_SYSTEM_PATH", lib, true);
        Environment.set_variable("GST_PLUGIN_SCANNER", lib + "/gst-plugin-scanner", true);

        Environment.set_variable("XDG_DATA_DIRS", res + "/share", true);
    }

    // Init all of the modules used
    Photon.init();
    Cellar.init();
    Halogen.init(args);

    Photon.UpdateCheck.get_instance();

    var floodlight = new Floodlight();
    return floodlight.run(args);
}

class Floodlight : Gtk.Application {
    public Floodlight() {
        Object(
            application_id: "io.gitlab.floodlight.Presenter",
            flags: ApplicationFlags.FLAGS_NONE
        );

        var about = new SimpleAction("about", null);
        about.activate.connect(() => {
            var info = new Glitter.TabInfo(ABOUT, null);
            Glitter.PresenterConsole.get_instance().tabs.show_tab(info);
        });
        this.add_action(about);

        var preferences = new SimpleAction("preferences", null);
        preferences.activate.connect(() => {
            var info = new Glitter.TabInfo(PREFERENCES, null);
            Glitter.PresenterConsole.get_instance().tabs.show_tab(info);
        });
        this.add_action(preferences);

        var help = new SimpleAction("help", null);
        help.activate.connect(() => {
            try {
                Gtk.show_uri_on_window(null, "https://floodlight.gitlab.io/presenter/usermanual", Gdk.CURRENT_TIME);
            } catch (Error e) {
                // not much we can do
            }
        });
        this.add_action(help);

        var quit = new SimpleAction("quit", null);
        quit.activate.connect(() => {
            Glitter.PresenterConsole.get_instance().close();
        });
        this.add_action(quit);
    }


    protected override void activate() {
        var console = new Glitter.PresenterConsole(this);
        console.show();

        if (this.inhibit(console, IDLE, "Preventing sleep because Floodlight Presenter is open") == 0) {
            debug("Platform does not support inhibiting idle");
        }

        Halogen.DisplayManager.get_instance().hide();
    }
}
